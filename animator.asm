//============================ ANIMATOR =======================

// default animation = 0
// dying animation = 1


.macro animation() {

	//toggleplexing shield / laser
	lda toggler	
	and #$01	
	bne laser	
	lda $d000
	sta $d002
	lda $d001
	sta $d003
	lda $d015	
	ora #%00000010	
	sta $d015	
	lda #%11111110	//all enemies and shield multicolor , ship not
	sta $d01c
	lda shieldActive
	beq !+
	lda toggler	
	and #$02	
	lsr	
	clc	
	adc #SHIELD_SPRITE	
	sta SPRITE_POINTERS + 1
	jmp rest
!:	lda toggler	
	and #$02	
	lsr	
	clc	
	adc #EXHAUST_SPRITE	
	sta SPRITE_POINTERS + 1
	jmp rest
laser:	
	lda #MISSILE_SPRITE
	sta SPRITE_POINTERS +1
	lda laserX
	sta $d002
	lda laserY
	sta $d003
	lda isFired
	beq !+
	lda $d015	
	ora #%00000010	
	sta $d015	
	jmp rest
!:	lda $d015	
	and #%11111101	
	sta $d015	
	lda #%11111100	//all enemies multicolor, ship and laser not
	sta $d01c
rest:
	lda animationCounter
	clc
	adc #$01
	cmp #ANIM_FRAME_SPEED
	beq !+
	sta animationCounter
	beq !+
	jmp anim_end

!:	lda #0
	sta animationCounter

//player death animation check

	lda playerDying
	bne !+
	jmp playerNotDying
	
!:	lda SPRITE_POINTERS
	cmp #SPRITE_POINTER_START
	bne !+
	lda #24  + SPRITE_POINTER_START
	sta SPRITE_POINTERS
	jmp anim_end
!:	cmp #27  + SPRITE_POINTER_START
	beq !+
	inc SPRITE_POINTERS 
	jmp anim_end
!:	
	ldx #2
!:	txa
	asl
	tay
	lda $d001,y
	sec
	sbc #$05
	sta $d001,y
	bcs notUp
	lda $d015
	and andBit,x
	sta $d015
notUp:
	inx
	cpx #8
	bne !-

	lda $d015
	and #%11111100
	sta $d015
	lda toggler
	and #$20
	bne !+
	jmp stillAnimating
!:	dec starfieldSpeed
	beq !+
	jmp stillAnimating
!:	
   .var doCheat = cmdLineVars.get("cheat")	
	.if(doCheat != "true") {
		jmp setupGameOverScreen
		//doDead()
	}
stillAnimating:	
	jmp anim_end		// really?	

playerNotDying:
	ldx #2

loop:	
	lda enemyDying,x
	bne dyingAnim
	lda enemyAlive,x
	bne !+
	jmp next
!:	lda SPRITE_POINTERS,x

//enemy 2 has 2 sprite types, going left and right
	cmp #ENEMY_SPRITE_2		// use pointer to define if it is type 2
	beq velocityCompare0	
	cmp #ENEMY_SPRITE_2 + 1
	bne doEor
	
	lda edx,x				// use dx value for direction 
	bmi goingRight1	
	lda #ENEMY_SPRITE_2 + 1
	jmp doEor
goingRight1:
	lda #ENEMY_SPRITE_2 +3
	jmp doEor

velocityCompare0:	
	lda edx,x	
	bmi goingRight0	
	lda #ENEMY_SPRITE_2
	jmp doEor
goingRight0:
	lda #ENEMY_SPRITE_2 +2
	jmp doEor
	
doEor:	
	eor #$01
	sta SPRITE_POINTERS,x
	
	
next:
	inx
	cpx #8
	bne loop
	jmp anim_end
dyingAnim:	

	lda SPRITE_POINTERS,x
	//ignore low bit (toggling animation)
	and #%11111110
	//if is 'base enemy sprite pointer' change to death anim pointer 
	cmp #ENEMY_SPRITE_1
	bne !+
	lda #ENEMY_SPRITE_1_DEATH
	sta SPRITE_POINTERS,x
	jmp next
!:	cmp #ENEMY_SPRITE_2
	bne !+
	lda #ENEMY_SPRITE_2_DEATH
	sta SPRITE_POINTERS,x
	jmp next
!:	cmp #ENEMY_SPRITE_3
	bne !+
	lda #ENEMY_SPRITE_3_DEATH
	sta SPRITE_POINTERS,x
	jmp next
!:	cmp #ENEMY_SPRITE_4
	bne !+
	lda #ENEMY_SPRITE_4_DEATH
	sta SPRITE_POINTERS,x
	jmp next
!:	lda SPRITE_POINTERS,x
	cmp #ENEMY_SPRITE_1_DEATH + 3
	bne !+
	jmp dead
!:	cmp #ENEMY_SPRITE_2_DEATH + 3
	bne !+
	jmp dead
!:	cmp #ENEMY_SPRITE_3_DEATH + 3
	bne !+
	jmp dead
!:	cmp #ENEMY_SPRITE_4_DEATH + 3
	bne !+
	jmp dead
!:	inc SPRITE_POINTERS,x 
	jmp next
dead:	

	lda #0
	sta enemyDying,x
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	jmp next
anim_end:
}