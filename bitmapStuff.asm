copyBitmap:
{
	lda #$34
	sta $01
	
	ldx #0

!:	lda colorSource,x
	sta COLOR_DATA,x
	lda colorSource + 250,x
	sta COLOR_DATA +250,x
	lda colorSource + 500,x
	sta COLOR_DATA+500,x
	lda colorSource + 750,x
	sta COLOR_DATA+750,x

	lda screenRamSource,x
	sta SCREEN_RAM_DATA,x
	lda screenRamSource + 250,x
	sta SCREEN_RAM_DATA +250,x
	lda screenRamSource + 500,x
	sta SCREEN_RAM_DATA+500,x
	lda screenRamSource + 750,x
	sta SCREEN_RAM_DATA+750 ,x

	inx
	cpx #250
	bne !-
	
	ldy #$20
loopOuter:	
	ldx #0
loopInner:	
	 lda bitmapSource,x	
	 sta BITMAP_IN_MEM,x	
	inx	
	bne loopInner	
	inc loopInner + 2	
	inc loopInner + 5
	dey
	bne loopOuter
	lda #$35
	sta $01
	rts
}

clearColorArea:
{
.var addAmount = 190
	ldx #0
	
	lda #32
!:	sta CHAR_DATA_TARGET,x
	sta CHAR_DATA_TARGET+addAmount,x
	sta CHAR_DATA_TARGET+addAmount*2,x
	sta CHAR_DATA_TARGET+addAmount*3,x
	sta $d800,x
	sta $d800+addAmount,x
	sta $d800+addAmount*2,x
	sta $d800+addAmount*3,x	
	inx
	cpx #addAmount
	bne !-
	rts
}
clearColorArea2:
{
.var addAmount = 250
	ldx #0
	
	lda #32
!:	sta CHAR_DATA_TARGET,x
	sta CHAR_DATA_TARGET+addAmount,x
	sta CHAR_DATA_TARGET+addAmount*2,x
	sta CHAR_DATA_TARGET+addAmount*3,x
	sta $d800,x
	sta $d800+addAmount,x
	sta $d800+addAmount*2,x
	sta $d800+addAmount*3,x	
	inx
	cpx #addAmount
	bne !-
	rts
}


clearArea:
{

	lda #$00
	sta sm0 + 1
	lda #$e0
	sta sm0 + 2
	lda #0
	ldy #25
loopOut:	
	ldx #0
loopIn:	
sm0:	
	sta $e000,x
	inx
	bne loopIn
	inc sm0+2
	dey 
	bne loopOut
	rts
}
// below Kernal/IO 
//Advanced Art Studio 2.0 (by OCP) (pc-ext: .ocp;.art)
//load address: $2000 - $471F
//$2000 - $3F3F	Bitmap
//$3F40 - $4327	Screen RAM
//$4328	Border
//$4329	Background
//$4338 - $471F	Color RAM 

clearBitmapAndCharColor:
{
//	lda #$34
//	sta $01
	jsr clearArea
//	lda #$35
//	sta $01

	jsr clearColorArea

	rts
}
clearBitmapAndCharColor2:
{
//	lda #$34
//	sta $01
	jsr clearArea
//	lda #$35
//	sta $01

	jsr clearColorArea2

	rts
}



//============ 		INTERRUPT BIT SHOULD BE SET =================================
copyBitmapPart: {
	txa
	asl
	asl
	asl
	tax
	stx smx
	sty smy
	
	lda #$34
	sta $01

	ldx smy: #10
loopOut:
	ldy #0
loopIn:	

	lda (BITMAP_TEMP_ZP),y
	sta (BITMAP_TEMP_ZP+2),y
	iny	
	cpy smx: #48	
	bne loopIn	
	lda BITMAP_TEMP_ZP	
	clc	
	adc #<320	
	sta BITMAP_TEMP_ZP	
	lda BITMAP_TEMP_ZP + 1
	adc #>320	
	sta BITMAP_TEMP_ZP +1

	lda BITMAP_TEMP_ZP +2
	clc	
	adc #<320	
	sta BITMAP_TEMP_ZP +2
	lda BITMAP_TEMP_ZP +3
	adc #>320	
	sta BITMAP_TEMP_ZP +3
	dex
	bne loopOut
	
	
	lda #$35
	sta $01

	rts
}
copyColorPart: {
	stx smx
	sty smy
	
	lda #$35	//don't have to read beneath IO
	sta $01

	ldx smy: #10
loopOut:
	ldy #0
loopIn:	
	lda (BITMAP_TEMP_ZP),y
	sta (BITMAP_TEMP_ZP+2),y

	lda (BITMAP_TEMP_ZP + 4),y
	sta (BITMAP_TEMP_ZP+6),y

	iny	
	cpy smx: #48	
	bne loopIn	
	lda BITMAP_TEMP_ZP	
	clc	
	adc #40	
	sta BITMAP_TEMP_ZP	
	lda BITMAP_TEMP_ZP + 1
	adc #0	
	sta BITMAP_TEMP_ZP +1

	lda BITMAP_TEMP_ZP +2
	clc	
	adc #40	
	sta BITMAP_TEMP_ZP +2
	lda BITMAP_TEMP_ZP +3
	adc #0	
	sta BITMAP_TEMP_ZP +3

	lda BITMAP_TEMP_ZP +4
	clc	
	adc #40	
	sta BITMAP_TEMP_ZP +4
	lda BITMAP_TEMP_ZP +5
	adc #0	
	sta BITMAP_TEMP_ZP +5

	lda BITMAP_TEMP_ZP +6
	clc	
	adc #40	
	sta BITMAP_TEMP_ZP +6
	lda BITMAP_TEMP_ZP +7
	adc #0	
	sta BITMAP_TEMP_ZP +7

	dex
	bne loopOut
	
	
	lda #$35
	sta $01

	rts
}