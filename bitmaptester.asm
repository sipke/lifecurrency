.var brkFile = createFile("debug.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

.pc = $0801 "Basic upstart"
:BasicUpstart($080d)		

.pc = $080d "setup" 

	lda $6328
	sta $d020
	lda $6329
	sta $d021 


	ldx #0
!:	 
.for (var i=750; i>=0; i-=250) {
	lda $4000+$2338+i,x
	sta $d800+i,x
}
	inx
	cpx #250
	bne !-
	
	ldx #0
!:	
.for (var i=750; i>=0; i-=250){
	lda $4000+$1f40+i,x	
	sta $7c00+i,x
}
	inx
	cpx #250
	bne !-
	
	

	
	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00

	lda $d011
	ora #%00100000
	sta $d011

	lda $d016
//	and #%11101111	//mc off
	ora #%00010000	//mc on
	sta $d016

	
	lda #%11110000
//	$d018	
//	and #%11100000	
//	ora #%11101000	//bit 3: bitmap at 2nd 8K, bit 4-7: last 1K of char 	
	sta $d018

loop:
	lda $dc00
	and #%00010000
	bne !+

	lda $d018
	clc
	adc #$10
	sta $d018
!:	
	jmp loop
.import source "bitmapStuff.asm"

*=$4000-2
	.import binary "resources/bitmap2.ocp"
	