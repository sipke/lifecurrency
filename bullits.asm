/*
		will destroy x,y and a
*/
///==================== SPAWN BULLIT ===============================
.macro spawnBullit(){
!:	
	ldy freeBullit
	cpy #NO_FREE_BULLIT
	bne !+
	jmp end
!:
	lda curEnemy
	asl
	tax	

	sty curBullit
	// sprite position is basis for x and y value
	// first check if y pos of enemy is within bounds to fire

	lda $d001,x
	sec 
	sbc #30
	bcs !+
	jmp end		// too low
!:	cmp #180 
	bcc posOK
	jmp end	// too high to fire bullit
	
posOK:	
	sta bullitY,y
	lda #1
	sta bullitActive,y
	lda $d000,x
	sta bullitX,y
	
	
	bullitPosToZP()

// set bullit speed	
	ldy curBullit
	lda #3
	sta bullitSpeedY,y

	
// x speed dependent on type

	lda enemyType,x
	tay
	lda bullitTypePerType,y
	cmp #BULLIT_DOWN
	beq bullitDown
	cmp #BULLIT_RANDOM
	bne aimedBullit
// bullitRandom
	getRandom()
	and #$0f
	tax
	lda randomBullitDistribution,x
	ldy curBullit
	sta bullitSpeedX,y	
	jmp doneShooting
	
//no x direction just down	
bullitDown:
	lda #$0	
	sta bullitSpeedX,y	
	jmp doneShooting

//aimedBullit	
aimedBullit:	
	ldy curBullit
	lda curEnemy
	asl
	tax	

	lda $d000
	sec
	sbc $d000,x
	bcs shootRight
shootLeft:	
	and #%01111111	//remove negative sign	
	cmp #$40
	bcc !+	
	lda #$fe	
	sta bullitSpeedX,y	
	jmp doneShooting
!:	cmp #$20
	bcc !+	
	lda #$ff	
	sta bullitSpeedX,y	
	jmp doneShooting
!:	//cmp #$10
	//	bcc !+	
	lda #$0	
	sta bullitSpeedX,y	
	jmp doneShooting
shootRight:	
	cmp #$40
	bcc !+	
	lda #$02	
	sta bullitSpeedX,y	
	jmp doneShooting
!:	cmp #$20
	bcc !+	
	lda #$01	
	sta bullitSpeedX,y	
	jmp doneShooting
!:	//cmp #$10
	//	bcc !+	
	lda #$0	
	sta bullitSpeedX,y	
doneShooting:
	lda #NO_FREE_BULLIT		// this index is no longer free 
	sta freeBullit 
end:
}

/*			CONVERT BULLIT X/Y to SCREEN POSITIONS
			Y SHOULD CONTAIN BULLIT INDEX				*/
			
///==================== BULLIT POS -> ZP  ===============================			
.macro bullitPosToZP(){	
	lda bullitX,y
	lsr
	lsr
	lsr
	sta sm1 +1		// add this later to bullit position
	sta bullitCharXTmp
	
	lda bullitY,y
	lsr
	lsr
	lsr
	sta bullitCharYTmp
	tay		// divide by 8 to get char position row

	lda curBullit		// bullit index x2 to get index in zero page 
	asl
	tax

// store char/col position in zp
	lda bullitDrawOffsetHi,y
	sta BULLIT_ZP + 1,x
	and #$03
	clc
	adc #$d8
	sta BULLIT_COL_ZP + 1,x
	lda bullitDrawOffsetLo,y
sm1:	
	adc #$00
	sta BULLIT_ZP,x
	sta BULLIT_COL_ZP,x
	bcc !+
	inc BULLIT_ZP + 1,x
	inc BULLIT_COL_ZP + 1,x
!:

}
///==================== RESTORE CHAR AND COLOR ===============================
.macro restoreCharAndCol() {

	lda curBullit
	asl
	tax
	lda BULLIT_ZP + 1,x
	clc
	and #$03
	adc #>STARS_CHARS_AT
	sta sm1 +2
	adc #$04
	sta sm2 +2
	lda BULLIT_ZP,x
	sta sm1 + 1
	sta sm2 + 1
sm1:	
	lda $4444 
	sta (BULLIT_ZP,x)
sm2:	
	lda $8888 
	sta (BULLIT_COL_ZP,x)
}

///==================== ADD SPEED TO POS ===============================
.macro addSpeedToPosition(end){
	// add speedy to y pos	
	ldy curBullit	
	lda bullitSpeedY,y
	clc
	adc bullitY,y
	cmp #BULLIT_Y_MAX 
	bcc !+
// out of bounds: remove	
	lda #0
	sta bullitActive,y
	jmp end
!:	sta bullitY,y
	// add speed x to x pos 
	// TODO ================== out of bounds x============================ TODO
	lda bullitSpeedX,y
	clc
	adc bullitX,y
	sta bullitX,y
}

///==================== UPDATE POS ===============================
.macro 	updatePosition(end) {
	addSpeedToPosition(end)
	bullitPosToZP()

	ldy curBullit
	lda bullitCharXTmp
	cmp playerCharPosX1
	bne checkX2
	lda bullitCharYTmp
	cmp playerCharPosY1
	beq hit
	cmp playerCharPosY2
	beq hit
checkX2:
	lda bullitCharXTmp
	cmp playerCharPosX2
	bne noHit
	lda bullitCharYTmp
	cmp playerCharPosY1
	beq hit
	cmp playerCharPosY2
	bne noHit
hit:
	inc bullitHits	//hitsplayer, don't draw, remove
	lda #0
	sta bullitActive,y 
	jmp updatePosEnd 
noHit:
	lda bullitX,y
	lsr
	and #$03
	sta sm1 +1
	lda bullitY,y
	lsr
	and #$03
	asl
	asl
	clc
sm1:
	adc #00
	adc #BULLIT_CHAR_START
	sta (BULLIT_ZP,x)	
	lda #WHITE	
	sta (BULLIT_COL_ZP,x)	
updatePosEnd:	
}

.macro updateBullits(){
	
	ldy #0
	sty bullitHits		//reset bullit hits 
	sty curBullit		// start bullit index
bloop:	

	ldy curBullit
	lda bullitActive,y
	bne !+
	sty freeBullit
	jmp next
!:
	restoreCharAndCol()
	updatePosition(next)

next:	
	ldy curBullit	
	iny
	cpy #MAX_NR_BULLITS
	beq !+
	sty curBullit
	jmp bloop	
!:
end: 
}
