.import source "vars.asm"
*=$8000
.word launcher //cold start
.word launcher //warm start
.byte $c3	//c
.byte $c2	//b
.byte $cd	//m
.byte $38	//8
.byte $30	//0

launcher:

  stx $d016
  jsr $fda3	//prepare irq
  jsr $fd50	//init memory
  jsr $fd15	//init i/o
  jsr $ff5b	//init video
  lda $02A6			//NTSC PAL check is reliable enough in cartridge
  eor #$01			// I want a 1 if true which is opposite of normal operation
  sta NTSC

	ldx #0
	stx $d020
	stx $d021
!:	lda screen_001,x
	sta $0400,x
	lda screen_001+1000,x
	sta $d800,x
	lda screen_001+250,x
	sta $0400+250,x
	lda screen_001+1000+250,x
	sta $d800+250,x
	lda screen_001+500,x
	sta $0400+500,x
	lda screen_001+1000+500,x
	sta $d800+500,x
	lda screen_001+750,x
	sta $0400+750,x
	lda screen_001+1000+750,x
	sta $d800+750,x
	inx
	cpx #250
	bne !-
	

                //make sure this sets up everything you need,
                //the calls above are probably sufficient
  ldx #$fb
  txs

//set up starting code outside of cartridge-area
move_starter:
  ldx #(starter_end-starter_start)
loop1:
  lda starter_start,x
  sta $100,x
  dex
  bpl loop1
  jmp $100
//---------------------------------
starter_start:	
  ldx #$40 //64 pages = 256 * 64 = 16384 Bytes
  ldy #0
loop:
src:
  lda exomized_data,y
dst:
  sta $801,y
  iny
  bne loop
  inc src+2-starter_start+$100 
  inc dst+2-starter_start+$100
  dex
  bpl loop

//make sure settings for $01 and IRQ etc are correct for your code
//remember THIS table from AAY64:

//       Bit+-------------+-----------+------------+
//       210| $8000-$BFFF |$D000-$DFFF|$E000-$FFFF |
//  +---+---+-------------+-----------+------------+
//  | 7 |111| Cart.+Basic |    I/O    | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 6 |110|     RAM     |    I/O    | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 5 |101|     RAM     |    I/O    |    RAM     |
//  +---+---+-------------+-----------+------------+
//  | 4 |100|     RAM     |    RAM    |    RAM     |
//  +---+---+-------------+-----------+------------+
//  | 3 |011| Cart.+Basic | Char. ROM | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 2 |010|     RAM     | Char. ROM | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 1 |001|     RAM     | Char. ROM |    RAM     |
//  +---+---+-------------+-----------+------------+
//  | 0 |000|     RAM     |    RAM    |    RAM     |
//  +---+---+-------------+-----------+------------+

  lda #$37 //cart is always on instead of BASIC unless it can be switched off via software
  sta $01
  jmp $80d //for exomizer, i.e.

starter_end:
//----------------------------------
exomized_data:
	.var program = LoadBinary("compressed.prg", BF_C64FILE)
	.fill program.getSize(), program.get(i)
	//.import binary "compressed.prg"
//syntax for exomizer 2.0.1:
//exomizer sfx sys game.prg -o data.exo

/*
		// if memory left add a petscii here

*/
.import source "resources/unpackpic.asm"

main_file_end:
//fill up full $4000 bytes for bin file ($c000-$8000=$4000)
	.fill ($c000-main_file_end),0
