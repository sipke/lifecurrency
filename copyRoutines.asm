copyAndSetupMem:


	//copy char set
{	
	ldx #0
!:	lda charSource,x
	sta CHARS_AT,x
	lda charSource+256,x
	sta CHARS_AT+256,x
	inx
	bne !-


	ldy #8
loopOuter:	
	ldx #0
loopInner:	
	lda bullitChars,x	
	sta CHARS_AT + BULLIT_CHAR_START*8,x	
	inx	
	bne loopInner	
	inc loopInner + 2	
	inc loopInner + 5
	dey
	bne loopOuter
	
	ldx #0
!:	lda menuchars,x
	sta CHARS_AT + 27*8,x
	inx
	cpx #10*8
	bne !-

	ldx #0
!:	lda metalchars,x
	sta CHARS_AT + METAL_CHARS_START*8,x
	inx
	cpx #20*8
	bne !-

.const METAL_BORDER_CHAR = 35
.const METAL_CHAR = 36


	//set star chars, 16 in number
	ldx #127
	lda #0
!:	sta FIRST_STAR_CHAR_BYTE,x
	dex 
	bpl !-
	lda #$10
	sta FIRST_STAR_CHAR_BYTE //tmpCharByte


	
}
//----------- copy sprites
{	
	ldy #ceil(NR_SPRITES*64/256)
loopOuter:	
	ldx #0
loopInner:	
	lda sprites,x	
	sta SPRITE_START,x	
	inx	
	bne loopInner	
	inc loopInner + 2	
	inc loopInner + 5
	dey
	bne loopOuter
}
	rts
	
//----------------- copy music ------------------

copyMusic:
{

	ldy #MUSIC_BLOCK_LENGTH
loopOuter:	
	ldx #0
loopInner:	
	 lda music,x	
	 sta MUSIC_START,x	
	inx	
	bne loopInner	
	inc loopInner + 2	
	inc loopInner + 5
	dey
	bne loopOuter
	rts
}
