/*======================================================================
					UPDATE ENEMIES
======================================================================= */

updateEnemies:

	lda newWave

	beq !+

	jmp setUpNewWave
	
//=========================================================================	

!:	ldx #2
	lda enemyInWave,x
	bne !+
	jmp do1
!:
	lda patternDelay,x
	beq !+
	
	dec patternDelay,x
	bne do1
	lda $d015
	ora orBit,x
	sta $d015
	lda #1
	sta enemyAlive,x
	jmp do1
!:	dec efc + 2		// frame count 0? get new dx and dy value
	beq !+
	jmp do1
!:	ldy enemyPatternCounter + 2
	iny 
	sty enemyPatternCounter + 2
epsmF0:	
	lda $bff0,y
	sta efc,x
	bne epsmX0		
	// end of pattern for enemy
	lda #0
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	// check if is last enemy
	lda nrEnemiesEnded
	clc
	adc #$01
	sta nrEnemiesEnded
	cmp nrEnemiesInWave
	bne !+	
	lda #1	
	sta newWave	
	jmp endEnemy
!:	jmp do1
	
epsmX0:
	lda $bff0,y
	sta edx,x
epsmY0:	
	lda $bff0,y
	sta edy,x
// ===============================================================================	
do1:
//=========================================================================	
!:	ldx #3
	lda enemyInWave,x
	bne !+
	jmp do2
!:	lda patternDelay,x
	beq !+
	dec patternDelay,x
	bne do2
	lda $d015
	ora orBit,x
	sta $d015
	lda #1
	sta enemyAlive,x
	jmp do2

!:	dec efc + 3		// frame count 0? get new dx and dy value
	beq !+
	jmp do2
!:	ldy enemyPatternCounter + 3
	iny 
	sty enemyPatternCounter + 3
epsmF1:	
	lda $bff0,y
	sta efc,x
	bne epsmX1		
	// end of pattern for enemy
	lda #0
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	// check if is last enemy
	lda nrEnemiesEnded
	clc
	adc #$01
	sta nrEnemiesEnded
	cmp nrEnemiesInWave
	bne !+	
	lda #1	
	sta newWave	
	jmp endEnemy
!:	jmp do2
	
epsmX1:
	lda $bff0,y
	sta edx,x
epsmY1:	
	lda $bff0,y
	sta edy,x
// ===============================================================================	
do2:
//=========================================================================	
!:	ldx #4
	lda enemyInWave,x
	bne !+
	jmp do3
!:	lda patternDelay,x
	beq !+
	dec patternDelay,x
	bne do3
	lda $d015
	ora orBit,x
	sta $d015
	lda #1
	sta enemyAlive,x
	jmp do3
!:
	dec efc + 4		// frame count 0? get new dx and dy value
	beq !+
	jmp do3
!:	ldy enemyPatternCounter + 4
	iny 
	sty enemyPatternCounter + 4
epsmF2:	
	lda $bff0,y
	sta efc,x
	bne epsmX2		
	// end of pattern for enemy
	lda #0
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	// check if is last enemy
	lda nrEnemiesEnded
	clc
	adc #$01
	sta nrEnemiesEnded
	cmp nrEnemiesInWave
	bne !+	
	lda #1	
	sta newWave	
	jmp endEnemy
!:	jmp do3
	
epsmX2:
	lda $bff0,y
	sta edx,x
epsmY2:	
	lda $bff0,y
	sta edy,x
// ===============================================================================	
do3:
//=========================================================================	
!:	ldx #5
	lda enemyInWave,x
	bne !+
	jmp do4
!:	lda patternDelay,x
	beq !+
	dec patternDelay,x
	bne do4
	lda $d015
	ora orBit,x
	sta $d015
	lda #1
	sta enemyAlive,x
	jmp do4
!:
	dec efc + 5		// frame count 0? get new dx and dy value
	beq !+
	jmp do4
!:	ldy enemyPatternCounter + 5
	iny 
	sty enemyPatternCounter + 5
epsmF3:	
	lda $bff0,y
	sta efc,x
	bne epsmX3		
	// end of pattern for enemy
	lda #0
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	// check if is last enemy
	lda nrEnemiesEnded
	clc
	adc #$01
	sta nrEnemiesEnded
	cmp nrEnemiesInWave
	bne !+	
	lda #1	
	sta newWave	
	jmp endEnemy
!:	jmp do4
	
epsmX3:
	lda $bff0,y
	sta edx,x
epsmY3:	
	lda $bff0,y
	sta edy,x
// ===============================================================================	
do4:
//=========================================================================	
	ldx #6	
	lda enemyInWave,x
	bne !+
	jmp do5
!:	lda patternDelay,x
	beq !+
	dec patternDelay,x
	bne do5
	lda $d015
	ora orBit,x
	sta $d015
	lda #1
	sta enemyAlive,x
	jmp do5
!:  dec efc + 6		// frame count 0? get new dx and dy value
	beq !+
	jmp do5
!:	ldy enemyPatternCounter + 6
	iny 
	sty enemyPatternCounter + 6
epsmF4:	
	lda $bff0,y
	sta efc,x
	bne epsmX4		
	// end of pattern for enemy
	lda #0
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	// check if is last enemy
	lda nrEnemiesEnded
	clc
	adc #$01
	sta nrEnemiesEnded
	cmp nrEnemiesInWave
	bne !+	
	lda #1	
	sta newWave	
	jmp endEnemy
!:	jmp do5
	
epsmX4:
	lda $bff0,y
	sta edx,x
epsmY4:	
	lda $bff0,y
	sta edy,x
// ===============================================================================	
do5:
//=========================================================================	
	ldx #7
	lda enemyInWave,x
	bne !+
	jmp moveEnemies
!:	lda patternDelay,x
	beq !+
	dec patternDelay,x
	lda $d015
	ora orBit,x
	sta $d015
	lda #1
	sta enemyAlive,x
	jmp moveEnemies

!:	dec efc + 7		// frame count 0? get new dx and dy value
	beq !+
	jmp moveEnemies
!:	ldy enemyPatternCounter + 7
	iny 
	sty enemyPatternCounter + 7
epsmF5:	
	lda $bff0,y
	sta efc,x
	bne epsmX5		
	// end of pattern for enemy
	lda #0
	sta enemyAlive,x
	lda $d015
	and andBit,x
	sta $d015
	// check if is last enemy
	lda nrEnemiesEnded
	clc
	adc #$01
	sta nrEnemiesEnded
	cmp nrEnemiesInWave
	bne !+	
	lda #1	
	sta newWave	
	jmp endEnemy
!:	jmp moveEnemies
	
epsmX5:
	lda $bff0,y
	sta edx,x
epsmY5:	
	lda $bff0,y
	sta edy,x
// ===============================================================================	


moveEnemies: 

	ldx #2 //lowest 
	stx curEnemy
enemyLoop:


movement:	
	ldx curEnemy
	lda enemyInWave,x
	bne !+
	jmp next
!:	lda enemyAlive,x	
	bne !+
	jmp next 	
!:	lda enemyDying,x	// if dying don't fire bullits or move etc.
	beq !+
	jmp next		
!:		

//pos update from pattern		
//=================Y=========================		
!:	lda edy,x
	bpl addingEY
	eor #$ff
	clc
	adc #$01
	sta sm3+1
	lda POSYLO,x
	sec
sm3: sbc #$00	
	sta POSYLO,x	
	bcs !+	
	dec POSYHI,x	
!:	
	jmp doPosx 	
addingEY:		
	clc		
	adc POSYLO,x		
	bcc !+		
	inc POSYHI,x			
!:	sta POSYLO,x		
doPosx:	
//======================= X======================

jmp patternX 
	
	lda enemyType,x
	tay
	lda movementPerType,y
	cmp #ENEMY_MOVE_PATTERN_AND_FOLLOW
	beq !+
	jmp patternX
!:	
	lda $d000,x
	cmp $d000
	bcc headLeft
headRight:
	lda POSXLO,x	
	clc	
	adc #$02	
	sta POSXLO,x	
	bcc !+	
	inc POSXHI,x	
!: jmp patternX 	
headLeft:
	lda POSXLO,x	
	sec	
	sbc #$02	
	sta POSXLO,x	
	bcs !+	
	dec POSXHI,x	
!:  	
//pos update from pattern
patternX:
	lda edx,x
	bpl addingEX
	eor #$ff
	clc
	adc #$01
	sta sm4+1
	lda POSXLO,x
	sec
sm4: sbc #$00	
	sta POSXLO,x	
	bcs !+	
	dec POSXHI,x	
	jmp doEnemyPosUpdate 	
addingEX:		
	clc		
	adc POSXLO,x		
	bcc !+		
	inc POSXHI,x			
!:	sta POSXLO,x		


doEnemyPosUpdate:
	txa
	asl
	tay
	lda POSXHI,x	
	lsr	
	lda POSXLO,x
	ror 	
	sta $d000,y	

	lda POSYHI,x	
	lsr	
	lda POSYLO,x
	ror 	
	sta $d001,y	
	
enemyFire:	
	ldx curEnemy
	getRandom()
	cmp bullitChance,x
	bcc !+
	jmp next
!:	
spawnBullit()

next:
	ldx curEnemy
	inx
	cpx #8
	bne !+
	jmp endEnemy
!:	stx curEnemy
	jmp enemyLoop		

endEnemy:

	rts

/*
						SETUP NEW WAVE
*/
setUpNewWave:
{
//get wave definition  pointer

	lda #%00000011
	sta $d015
	lda #0
	.for (var i=2; i<8; i++){
		sta enemyAlive + i
		sta enemyDying + i
		sta enemyInWave + i
	}


	ldx currentWave
	inx
	cpx nrOfWaves
	bne !+
//TODO remove looping waves ?

	ldx #0
!:	stx currentWave
	
	lda waveDefinitionPointerLo,x 
	sta ENEMY_ZP_TMP0
	lda waveDefinitionPointerHi,x 
	sta ENEMY_ZP_TMP1
//now read wave properties	
	ldy #0
	lda (ENEMY_ZP_TMP0),y
	sta nrEnemies	//zero indexed
	clc
	adc #$01
	sta nrEnemiesInWave //one indexed
	iny
	ldx #0
!:	lda (ENEMY_ZP_TMP0),y
	sta enemyPattern + 2,x
	lda #1
	sta enemyAlive + 2,x
	inx
	iny
	cpx nrEnemiesInWave
	bne !-  
//pattern start index	
	ldx #0
	lda #0
	sta enemyPatternCounter + 2,x
!:	lda (ENEMY_ZP_TMP0),y
	sta patternDelay + 2,x
	inx
	iny
	cpx nrEnemiesInWave
	bne !-  
//enemy type	
	ldx #0
!:	lda (ENEMY_ZP_TMP0),y
	sta enemyType + 2,x
	inx
	iny
	cpx nrEnemiesInWave
	bne !-
  
//xStart
	ldx #0
!:	lda (ENEMY_ZP_TMP0),y
	clc
	asl
	sta POSXLO + 2,x
	lda #0
	adc #$00
	sta POSXHI + 2,x
	inx
	iny
	cpx nrEnemiesInWave
	bne !-  
//yStart
	ldx #0
!:	lda (ENEMY_ZP_TMP0),y
	clc
	asl
	sta POSYLO + 2,x
	lda #0
	adc #$00
	sta POSYHI + 2,x
	inx
	iny
	cpx nrEnemiesInWave
	bne !-  

// now for all enemies read pattern and modify code so patternpointers are set	
// I do this naively and don't take into account if enemy is in wave or not
// I can do speedcode this way, which is a little faster, but also prevents 'index-hell' (get index to get index to pointer etc)

	ldx #2
	lda enemyPattern,x
	tax
	lda patternXPointerLo,x
	sta epsmX0 + 1
	lda patternXPointerHi,x
	sta epsmX0 + 2
	lda patternYPointerLo,x
	sta epsmY0 + 1
	lda patternYPointerHi,x
	sta epsmY0 + 2
	lda framesTillNextPointerLo,x
	sta epsmF0 + 1
	lda framesTillNextPointerHi,x
	sta epsmF0 + 2

	ldx #3
	lda enemyPattern,x
	tax
	lda patternXPointerLo,x
	sta epsmX1 + 1
	lda patternXPointerHi,x
	sta epsmX1 + 2
	lda patternYPointerLo,x
	sta epsmY1 + 1
	lda patternYPointerHi,x
	sta epsmY1 + 2
	lda framesTillNextPointerLo,x
	sta epsmF1 + 1
	lda framesTillNextPointerHi,x
	sta epsmF1 + 2

	ldx #4
	lda enemyPattern,x
	tax
	lda patternXPointerLo,x
	sta epsmX2 + 1
	lda patternXPointerHi,x
	sta epsmX2 + 2
	lda patternYPointerLo,x
	sta epsmY2 + 1
	lda patternYPointerHi,x
	sta epsmY2 + 2
	lda framesTillNextPointerLo,x
	sta epsmF2 + 1
	lda framesTillNextPointerHi,x
	sta epsmF2 + 2

	ldx #5
	lda enemyPattern,x
	tax
	lda patternXPointerLo,x
	sta epsmX3 + 1
	lda patternXPointerHi,x
	sta epsmX3 + 2
	lda patternYPointerLo,x
	sta epsmY3 + 1
	lda patternYPointerHi,x
	sta epsmY3 + 2
	lda framesTillNextPointerLo,x
	sta epsmF3 + 1
	lda framesTillNextPointerHi,x
	sta epsmF3 + 2

	ldx #6
	lda enemyPattern,x
	tax
	lda patternXPointerLo,x
	sta epsmX4 + 1
	lda patternXPointerHi,x
	sta epsmX4 + 2
	lda patternYPointerLo,x
	sta epsmY4 + 1
	lda patternYPointerHi,x
	sta epsmY4 + 2
	lda framesTillNextPointerLo,x
	sta epsmF4 + 1
	lda framesTillNextPointerHi,x
	sta epsmF4 + 2

	ldx #7
	lda enemyPattern,x
	tax
	lda patternXPointerLo,x
	sta epsmX5 + 1
	lda patternXPointerHi,x
	sta epsmX5 + 2
	lda patternYPointerLo,x
	sta epsmY5 + 1
	lda patternYPointerHi,x
	sta epsmY5 + 2
	lda framesTillNextPointerLo,x
	sta epsmF5 + 1
	lda framesTillNextPointerHi,x
	sta epsmF5 + 2

//changed: now reveal when delay is zero
//	ldx nrEnemiesInWave
//	dex
//	lda $d015 
//	ora nrEnemyAliveOrValue,x
//	sta $d015

//let the pattern position them automatically
	ldx #0
setAlive:	
	lda #$01
	sta efc + 2,x		//next frame this will become zero, so next frame will read next step in pattern
	lda enemyPatternCounter +2,x
	sec
	sbc #$01
	sta enemyPatternCounter +2,x	//next step will then default to the original start value
	lda #1
	cpx nrEnemiesInWave
	bcc !+
	lda #0
!:	sta enemyInWave + 2,x
	lda #0	
	sta enemyDying + 2,x
	sta enemyAlive+2,x
	inx
	cpx #6	
	bne setAlive

.for (var i=2; i<8; i++){
	ldx enemyType + i
	lda spritePointerPerType,x
	sta SPRITE_POINTERS +i
	lda spriteColorPerType,x
	sta $d027 + i
	lda bullitChancePerType,x
	sta bullitChance + i
// not very efficient oh well
	lda POSXHI + i	
	lsr	
	lda POSXLO +i
	ror 	
	sta $d000 + i*2	

	lda POSYHI + i	
	lsr	
	lda POSYLO + i
	ror 	
	sta $d001 + i*2	
}

	lda #0
	sta newWave
	sta nrEnemiesEnded
end:	
	rts
}	
