// enemy types 
//                          MAX 16 !!!!--------------------------
// if a random value between 0-256 is below this value a bullit will be fired
bullitChancePerType:
		.byte 1,25,70,90
		.byte 0,0,0,0
		.byte 0,0,0,0
		.byte 0,0,0,0
// type 1 is the dog face, type 2 is the sausage dog 		
spritePointerPerType:
		.byte ENEMY_SPRITE_3,ENEMY_SPRITE_4,ENEMY_SPRITE_1,ENEMY_SPRITE_2
		.byte ENEMY_SPRITE_1,ENEMY_SPRITE_2,ENEMY_SPRITE_1,ENEMY_SPRITE_2
		.byte ENEMY_SPRITE_1,ENEMY_SPRITE_2,ENEMY_SPRITE_1,ENEMY_SPRITE_2
		.byte ENEMY_SPRITE_1,ENEMY_SPRITE_2,ENEMY_SPRITE_1,ENEMY_SPRITE_2
spriteColorPerType:		
		.byte WHITE,RED,GREEN,BLUE		
		.byte WHITE,RED,GREEN,BLUE		
		.byte WHITE,RED,GREEN,BLUE		
		.byte WHITE,RED,GREEN,BLUE		
		// bullits always go 3/2 pixels down per frame 
		// random is -3,-2,-1,0, 1,2,3 in horizontal direction
		//distributed like -3,-2,-2,-2,-1,-1,-1,0,0,1,1,1,2,2,2,3 		
bullitTypePerType:	//BULLIT_DOWN, BULLIT_FOLLOW, BULLIT_RANDOM
		.byte BULLIT_DOWN,BULLIT_DOWN,BULLIT_FOLLOW,BULLIT_FOLLOW
		.byte BULLIT_RANDOM,BULLIT_DOWN,BULLIT_FOLLOW,BULLIT_FOLLOW
		.byte BULLIT_RANDOM,BULLIT_DOWN,BULLIT_FOLLOW,BULLIT_FOLLOW
		.byte BULLIT_RANDOM,BULLIT_DOWN,BULLIT_FOLLOW,BULLIT_FOLLOW
movementPerType:	//ENEMY_MOVE_PATTERN_AND_FOLLOW,ENEMY_MOVE_PATTERN
		.byte ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN_AND_FOLLOW,ENEMY_MOVE_PATTERN
		.byte ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN_AND_FOLLOW,ENEMY_MOVE_PATTERN
		.byte ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN_AND_FOLLOW,ENEMY_MOVE_PATTERN
		.byte ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN,ENEMY_MOVE_PATTERN_AND_FOLLOW,ENEMY_MOVE_PATTERN
		
scorePerType:		
	// because of decimal mode do not get above 99
		.byte 10,20,30,40
		.byte 10,20,30,40
		.byte 10,20,30,40
		.byte 10,20,30,40 		

killsPerType:
		.fill 16,0
		
randomBullitDistribution:
		.byte -3,-2,-2,-2,-1,-1,-1,0,0,1,1,1,2,2,2,3
