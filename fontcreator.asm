//create a cursive font

// inverse all except corners

copyAndAlterFont:
{

		lda $01
		and #%11111011
		sta $01
loop0:	ldx #0
loop1:	
		txa
		and #$07
		cmp #4
		bcc !+
		lda #ASL
		jmp sm1 -3
!:		lda #NOP
		sta	sm1 + 3  
sm1:	lda $d000,x
		asl
sm2:	sta CHARS_AT,x 


/*
		lda #$ff
		sta av +1
		sta ev +1
		lda #$00
		sta ov +1
		txa
		and #$07
		tay
		beq conv1
		cpy #7
		beq conv1
		jmp fntconv
conv1: 
	
		lda #%01111110
		sta av + 1

fntconv:		
*/		
//sm1:	lda $d000,x
//ev:		eor #$ff
//ov:		ora #$00
//av:		and #$ff 
//sm2:	sta CHARS_AT,x

		inx
		bne loop1
		inc sm1 + 2
		inc sm2 + 2
		dec font_blocks
		bne loop0
		
		ldx #7
		lda #0
!:		sta CHARS_AT + 32*8,x
		dex
		bpl !-
		
		lda $01
		ora #%00000100
		sta $01
		
	
		rts
	
font_blocks: .byte 2		
}		
