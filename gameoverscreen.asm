
setupGameOverScreen:
	lda #0
	sta doPrint

	sei

	// turn off screen
	lda $d016
	and #%11011111
	sta $d016

	lda #0
	sta $d015
	
	ldx #0
	
!:			

	lda #WHITE			
		
	sta $d800, x
	sta $d800+250,x
	sta $d800+500,x
	sta $d800+750,x
	//eor YELLOW
	inx 
	cpx #250
	bne !-

	jsr clearBitmapAndCharColor
	
	generateCopyCode("helmet")		
	generateCopyCode("game")		
	generateCopyCode("over")


//	turn on screen
	lda $d016
	ora #%00100000
	sta $d016
		
/*======================================================			
			SETUP IRQ			
====================================================*/

	lda #<go_irq_top
	sta $fffe
	lda #>go_irq_top
	sta $ffff
	lda #<freeze
	sta $fffa
	lda #>freeze
	sta $fffb
	
	lda #80 
	sta $d012			// raster interrupt at  line 										
	lda #27
	sta $d011										
/*
    ldx #$00										
    stx $dc0e
    inx
    stx $d01a			// enable raster interrupt 
	
	cli
*/	
	jmp irq_end 


initIntro2:
	lda #$2F
	sta coolDownCount

	jsr setupStartScreen//initMenu //initGameScreen
	jmp irq_end

go_irq_top:

	enterIrq()
	
	lda $dc01
	and #$10
	beq initIntro2

	jsr MUSIC_START + 3
	
	ldx #>go_irq_bottom
	ldy #<go_irq_bottom
	stx $FFFF
	sty $FFFE
	lda #180
	sta $D012
	lda $d011
	and #$7f
	sta $d011
	
	lda $dd00
	and #%11111100
	sta $dd00

	lda $d011
	and #%01111111	// irq line bit 9 =0
	ora #%00100000
	sta $d011

	lda $d016
	ora #%00110000	//mc on, screen on
	sta $d016

	.assert "equal", ((mod(CHAR_DATA_TARGET,$4000)/$400) <<4) | %00001000, %11101000 

	lda #%11101000 //bit 3: bitmap at 2nd 8K, bit 4-7: second to last 1K of char
	sta $d018
	sec
	rol $D019
	jmp quitIrq

go_irq_bottom:	
	enterIrq()	

//theresrubbish at bottom of bitmap so turn off
	lda $d016
	and #%11011111
	sta $d016

	ldx #>go_irq_top
	ldy #<go_irq_top
	stx $FFFF
	sty $FFFE

	sec
	rol $D019
	jmp quitIrq
