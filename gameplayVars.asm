// MENU VARS

// THESE VALUES ARE IN DECIMAL MODE!!!
.const SHIELDS_PER_CREDIT = $16 // SO is 16 decimal ($10)
.const BULLITS_PER_CREDIT = $12 // SO is 12 decimal ($0c)
.const DIST_TO_TRAVEL = $1111	// max $9999

// gameplay vars
.const BULLIT_DOWN = 0
.const BULLIT_RANDOM = 1
.const BULLIT_FOLLOW = 2

.const ENEMY_MOVE_PATTERN = 0
.const ENEMY_MOVE_PATTERN_AND_FOLLOW = 1
.const ENEMY_MOVE_FOLLOW = 2


.const MAX_Y_ENEMY = 200;		// below this value enemy is removed? not sure if it's still used, patterns should make this unneccesasy
.const BLINK_FRAME_COUNT = 50	// nr of frames after hit , when blinking and invulnerable 

.const PLAYER_START_X =160		//player sprite coordinates
.const PLAYER_START_Y =200

.const NR_FRAMES_BEFORE_FIRST_WAVE = $f0	// nr of frames before waves arrive to prevent too hectic gameplay  
//			THESE FRAME DEPENDEND COUNTERS MUST BE 1, 3, 7, 15, 31, 63, 127 or 255 (so a simple AND x can be used)
.const SHIELD_DECREASE_EVERY = 31 //nr of frames counted before shield is decreased by 1  
.const DIST_TRAVELLED_INCREASE_EVERY = 7 //nr of frames counted before dist is increased by 1


// BECAUSE OF DECIMAL MODE IS EASIER IF NOT LARGER THAN 99 (1 byte) 
.const POINTS_PER_LIFE_LEFT = 99
.const POINTS_PER_BULLIT_LEFT = 5
.const POINTS_PER_SHIELD_LEFT = 10
.const POINTS_PER_CREDIT_LEFT = 25