

/*
	X cur enemy index
	changes Y
	changes A
*/
.macro doMisHitEnemy(){
	//lda #<sfx2
	//ldy #>sfx2
	//ldx #7
	
	//jsr MUSIC_START + 6

	lda #1
	sta enemyDying,x

	lda enemyType,x
	.for (var i=0; i<16; i++){
		cmp #i
		bne !+
		inc killsPerType + i
!:		
	}		

}
.macro missileHitEnemyTest() {
	lda enemyAlive,x
	bne !+
	jmp noHit
!:	lda isFired
	bne !+ 
	jmp noHit
!:	
	txa
	asl
	tay
	lda $d000,y
	cmp laserX //$d00e	//x mis < x enemy not hit
	bcc !+
	jmp noHit
!:	lda laserX //$d00e	//xmis - x enemy <21?
	sec
	sbc $d000,y
	cmp #21
	bcc !+
	jmp noHit
!:
	lda $d001,y
	cmp laserY //$d00f	//carry set if enemy y >= mis y 
	bcs ylarger
yless:	
	lda laserY//$d00f	//y mis - y enemy <16?
	sec
	sbc $d001,y
	cmp #21
	bcc !+
	jmp noHit
!:	
// is hit
	jmp doHit
	
ylarger:
	sec			//y enemy - y mis <16?
	sbc laserY//$d00f
	cmp #16
	bcs noHit
// is hit
doHit:
	doMisHitEnemy()
	
	
noHit:
}

.macro doPlayHitEnemy(){

	lda #1
	sta enemyDying,x

	lda blinking
	beq !+
	jmp end		// blinking: no damage

!:	lda shieldActive
	beq doDamage
	lda shieldsDec
	bne !+
	lda shieldsDec + 1
	bne !+
doDamage:	
	dec lives
	bne notdead

	lda #1
	sta playerDying
	jmp end
	
!:	decShield() 
	incBullit()
	jmp end
notdead:	 
	lda #1
	sta blinking
	lda #BLINK_FRAME_COUNT
	sta	blinkingCount
end:	


}


.macro playerHitEnemyTest() {
	lda enemyAlive,x
	bne !+
	jmp notHit

!:	txa
	asl
	tay
checkStart:
  
	lda $d001,y
	cmp #PLAYER_START_Y -21
	bcs !+
	jmp notHit	 
!:	cmp #PLAYER_START_Y + PLAYER_HEIGHT
	bcc !+
	jmp notHit
!:
	lda $d000,y
	cmp $d000
	bcc enemyLeftOfPlayer
enemyRightOfPlayer:
	sec
	sbc $d000
	cmp #PLAYER_WIDTH		
	bcs !+		
	jmp doHit
!:	jmp notHit
enemyLeftOfPlayer:
	lda $d000
	sec
	sbc $d000,y
	cmp #ENEMY_WIDTH
	bcc doHit
	jmp notHit
doHit:
	doPlayHitEnemy()
notHit:
}


/*=================================
			HITTESTS
=====================================*/			
{			
	
	ldx #2
loop:
	stx curEnemy
	missileHitEnemyTest()
	playerHitEnemyTest()
	ldx curEnemy
	inx
	cpx #8
	beq !+
	jmp loop
!:
	//----------- bullithits ------------
	ldx bullitHits
	beq noHits
	jmp playerHitByBullit

noHits:
endHitTest:
	jmp end
	

playerHitByBullit:
{

	lda blinking
	beq !+
	jmp end		// blinking: no damage
!:	lda shieldActive
	beq doDamage
 	decShield( )
	bcs enoughShields
doDamage:  
	lda #BLINK_FRAME_COUNT
	sta	blinkingCount
	lda #1
	sta blinking
	dec lives
	bne notDead

	lda #1
	sta playerDying
	jmp endHitTest
enoughShields:
	decBullit()

notDead:	 
end:	
	jmp endHitTest
}
	
	
	
shieldHit:	
	incBullit()	

	decShield()

	lda shieldsDec
	bne !+	
	lda shieldsDec + 1
	bne !+
	lda #0	
	sta shieldActive	
	
!:	rts
end:
}				