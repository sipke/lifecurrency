#import "libs/MyPseudoCommands.lib"
.var brkFile = createFile("debug.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}
.macro breakOnMemChange(addr) {
	.eval brkFile.writeln("break store" + toHexString(addr))
}

.import source "vars.asm"
.import source "macros.asm"
 
.pc = $0801 "Basic upstart"

:BasicUpstart($0810)		

.pc = $0810 "setup" 
	
	ldx #0
!:	txa
	asl
	tay
	getRandom()
	sta $d000,y
	getRandom()
	and #$31
	clc
	adc #(PLAYER_START_Y-31)

	sta $d001,y
	inx
	cpx #8
	bne !-
	
	lda #PLAYER_START_Y
	sta $d001
	
	lda #13
	sta 2040
	
	ldx #0
	ldy #255
	.for (var i=0; i<63; i++){
		.if(mod(i,3)==2 || i>=48) {
			stx 832+i
		}else{
			sty 832+i
		}
	}
	
	lda #$ff
	sta $d015
	

	jsr setupIrq
	jmp *


irq:

	lda $d019
	and #$01	//if raster is reason for interrupt
	bne do_irq	//do irq
	jmp $ea31	
do_irq:
	inc $d020

	jsr hittest


	lax $dc01
// TODO replace AND with BIT ?	
	and #%00000100
	bne checkright
// go left	
	dec $d000 
checkright:
	txa
	and #%00001000
	bne checkup
	inc $d000 
checkup:
	txa
	and #%00000001
	bne checkdown
	dec $d001
checkdown:	
	txa
	and #%00000010
	bne checkEnd
	inc $d001
checkEnd:

	


irq_end:
	dec $d020
	asl $d019	//acknowlegde irq			
	cli			
	jmp $ea81 


setupIrq:
	//setup irq
	lda #<irq
	sta $0314
	lda #>irq
	sta $0315
	lda #220
	sta $d012			// raster interrupt at  line 200										
	lda #27
	sta $d011										
    lda #$01										
    sta $d01a			// enable raster interrupt 
	rts

.import source "tables.asm"


// -----------------------------------------------------------------

hittest:


	ldx #2
	ldy #4
	break()
checkStart:
	lda $d001,y
	cmp #PLAYER_START_Y -21
	bcc notHit	 
	cmp #PLAYER_START_Y + PLAYER_HEIGHT
	bcs notHit

	lda $d000,y
	cmp $d000
	bcc enemyLeftOfPlayer
enemyRightOfPlayer:
	sec
	sbc $d000
	cmp #PLAYER_WIDTH		
	bcs notHit		
	jmp doHit
enemyLeftOfPlayer:
	lda $d000
	sec
	sbc $d000,y
	cmp #ENEMY_WIDTH
	bcs notHit
doHit:
	inc $d027,x	
notHit:		
	inx		
	iny		
	iny		
	cpx #8		
	bne checkStart		 
	rts
	
