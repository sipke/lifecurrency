.macro addCoverSprites() {

		lda #255
		sta $d015 // sprites off
		sta $d017	//expand
		sta $d01d 
		lda #0
		sta $d01b	//sprites in front of chars
		sta $d027	// color black
		sta $d028
		sta $d029
		sta $d02a
		sta $d02b
		sta $d02c
		sta $d02d
		sta $d02e
	
		lda #FILLED_SPRITE
		ldy #200
		.for(var i=0; i<8; i++){
			sta CHAR_DATA_TARGET + $03f8
			sty $d001 + i*2
			ldx #40+i*48
			stx $d000 + i *2
		}
		lda #%11100000
		sta $d010	//MSB

}

.macro enterIrq() {
		 pha
		 tya
		 pha
		 txa
		 pha
}

.macro exitIrq (intvector, rasterline) {
	.if(intvector != -1) {
		ldx #>intvector
		ldy #<intvector
		stx $FFFF
		sty $FFFE
		lda #rasterline
		sta $D012
	}
	sec
	rol $D019
	jmp quitIrq
	
}

.macro decShield() {
	sed
	lda shieldsDec + 1
	sec
	sbc #1
	sta shieldsDec +1
	lda shieldsDec
	bcs !+	// carry set: not negative no need to check if total negative
	beq !+	// if MSB already zero and negative bit set: set all to zero
	lda #0
	sta  bullitsDec + 1
	jmp skip
!:	sbc #0
skip:
	sta shieldsDec
	cld
}
.macro decBullit() {
	sed
	lda bullitsDec + 1
	sec
	sbc #1
	sta bullitsDec +1
	lda bullitsDec
	bcs !+	// carry set: not negative no need to check if total negative
	beq !+	// if MSB already zero and negative bit set: set all to zero
	lda #0
	sta  bullitsDec + 1
	jmp skip
!:	sbc #0
skip:	
	sta bullitsDec
	cld
}
.macro incShield() {
	sed
	lda shieldsDec + 1
	clc
	adc #1
	sta shieldsDec +1
	lda shieldsDec
	adc #0
	sta shieldsDec
	cld
}
.macro incBullit() {
	sed
	lda bullitsDec + 1
	clc
	adc #1

	sta bullitsDec +1
	lda bullitsDec
	adc #0
	sta bullitsDec
	cld
}


.macro doMissile(){
	lda isFired
	beq done
	
	lda laserY //$d00f
	sec
	sbc #MISSILE_SPEED
	sta laserY //$d00f
	bcs done
	lda #0
	sta isFired
	//lda $d015
	//and #%01111111
	//sta $d015
	
	decBullit()
	
done:
}
.macro fireMissile() { 

	lda isFired
	bne done
	lda bullitsDec
	bne !+
	lda bullitsDec + 1
	bne !+
	jmp done
 !:
	lda #1
	sta isFired

	lda $d000
	sta laserX //$d00e
	lda $d001
	sec
	sbc #16
	sta laserY //$d00f
	//lda $d015
	//ora #%10000000
	//sta $d015
done:
} 

.macro playerControls(){ 
/*---------------------------------------------
  player controls
---------------------------------------------*/
	lax $dc01
	and #%00000100
	bne checkright
// go left	
	lda SPEEDX 
	cmp #MIN_SPEED
	// already at min speed
	beq checkright
	dec SPEEDX
	jmp endjoy
checkright:
	txa
	and #%00001000
	bne checkup
	lda SPEEDX 
	cmp #MAX_SPEED
	// already at max speed
	beq checkup
	inc SPEEDX
	jmp endjoy
checkup:
	txa
	and #%00000001
	bne checkdown
	lda shieldsDec
	bne !+
	lda shieldsDec + 1
	bne !+
	jmp checkdown
!:	lda #1
	sta shieldActive

	jmp endjoy
checkdown:	
	txa
	and #%00000010
	bne checkButton
	lda #0
	sta shieldActive
	jmp endjoy
	
checkButton:	
	txa
	
	cmp prevJoyVal	//debounce
	sta prevJoyVal
	bne !+
	jmp endjoy
!:	
	
	and #%00010000
	bne endjoy
	lda bullitsDec
	bne !+
	lda bullitsDec +1
	bne !+
	jmp nofire
!:	fireMissile()
	lda #<sfx1
	ldy #>sfx1
	ldx #7
	jsr MUSIC_START + 6
nofire:

	
endjoy:	

} 
//FROM codebase64
.macro getRandom() {

		//lda randomSeed
		//lsr
		
sm:        lda randomSeed 
           beq doEor
           clc
           asl
           beq noEor    //if the input was $80, skip the EOR
           bcc noEor
doEor:     eor #$1d//B8//#$1d
noEor:     sta randomSeed

}

.macro updatePlayerPos()
{
/*-------------------------------------------

		                     =================== UPDATE PLAYER POSITION =============================

--------------------------------------------*/
	lda SPEEDX
	beq noposupdate
	
	bpl adding
	eor #$FF	
	clc	
	adc #$01	
	sta sm2 +1	
	lda POSXLO
	sec
sm2:	
	sbc #$00
	bcs !+
	dec POSXHI
!:  sta POSXLO
	jmp posupdate
adding:
	clc
	adc POSXLO
	bcc !+
	inc POSXHI
!:	sta POSXLO	
posupdate:	
	lda POSXHI	
	lsr	
	lda POSXLO
	ror 	
	sta $d000	
	//sta $d002
	//save char for bullit colllision
	sec
	sbc #$18
	lsr
	lsr
	lsr
	sta playerCharPosX1
	clc
	adc #$01
	sta playerCharPosX2

	//boundaries check
	lda $d000
	cmp #4
	bcc !+
	cmp #244
	bcs !+
	jmp noposupdate
!:	lda SPEEDX
	eor #$ff
	clc
	adc #$01
	sta SPEEDX
		
noposupdate:
	lda shieldActive
	beq noShield
	
	lda shieldDecreaseCounter
	clc
	adc #$01
	cmp #SHIELD_DECREASE_EVERY
	beq !+
	sta shieldDecreaseCounter 
	jmp noShield
!:	lda #0	
	sta shieldDecreaseCounter	
	lda shieldsDec
	bne !+
	lda shieldsDec + 1
	bne !+
	jmp noShield
!:	decShield()
	lda shieldsDec
	bne noShield
	lda shieldsDec + 1
	bne noShield
	// shield just become zero, set to non active
	lda #0
	sta shieldActive
noShield:
}

