#import "libs/MyPseudoCommands.lib"
.var brkFile = createFile("debug.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}
.macro breakOnMemChange(addr) {
	.eval brkFile.writeln("break store" + toHexString(addr))
}

.var isCartBuild = cmdLineVars.get("cart")


.import source "screencopyGenerator.asm"
.import source "vars.asm" 
.import source "macros.asm"
.import source "bullits.asm"
.import source "animator.asm"
.import source "printing.asm"

.pc = $0801 "Basic upstart"
:BasicUpstart($080d)		

.pc = $080d "setup" 

	sei
	// turn off basic and kernal
	lda #$35
	sta $01

	jsr copyMusic	// is second to last in memory has biggest chance of being overwritten
	jsr copyBitmap	// is last in memory has biggest chance of being overwritten
	jsr createStarField

	jsr copyAndSetupMem
restart:	
	lda #$35
	sta $01

	jsr setupStartScreen
	cli
loopOutsideIrq:	
!:	printVars()
	jsr scoreHandling
	jmp loopOutsideIrq
	.import source "startscreen.asm"
	.import source "scores.asm"
//-----------------------------------------------------------
	.import source "copyRoutines.asm"
	.import source "bitmapStuff.asm"

	.import source "gameoverscreen.asm"
	.import source "wintro.asm"
	.import source "scorescreen.asm"
	.import source "menu.asm"
//--------------------------------------------------------
	.import source "setupgame.asm"
	.import source "starfield.asm"
	.import source "enemyNew.asm"
	

irq:

	enterIrq()
	inc $d020
	lda gameEnded
	beq !+
	jmp irq_end
!:

	lda blinking
	beq endBlink
	
	dec blinkingCount
	bne doBlink
	lda #0
	sta blinking
	lda $d015
	ora #%00000001
	sta $d015
	lda #0
	sta shieldActive
	jmp endBlink
doBlink:	
	lda $d015	
	eor #%00000001	
	sta $d015	
endBlink:

	lda playerDying
	beq !+
	jmp always
!:
	doMissile()
	playerControls()
	updatePlayerPos()
	jsr updateEnemies
	updateBullits()

always:
	updateChars()
	animation()

//----------------		
	.import source "hittests.asm"
//----------------		


	sed
	lda distanceToTravelDec + 1
	sec
	sbc #$01
	sta distanceToTravelDec + 1
	lda distanceToTravelDec
	sbc #$00
	sta distanceToTravelDec

	cld
	lda distanceToTravelDec
	bne !+
	lda distanceToTravelDec + 1
	bne !+
	doWin()
!:	
		


//start first wave after countdown	
	lda countDown
	beq !+
	dec countDown
	bne !+ 
	lda #1
	sta newWave
!:


irq_end:
	
	jsr MUSIC_START + 3
	inc toggler
	inc irqDone
	dec $d020
	exitIrq(-1,-1)
	
quitIrq:
	pla
	tax
	pla
	tay
	pla
freeze:
	rti 

// A = HB address of text
// Y =  LB address of text
print:
	sta TXTOUT_HB	// store on HB Text pointer
	sty TXTOUT_LB	// store on HB Text pointer
	ldy #$00        // set index to zero
t0:	lda (TXTOUT_LB),y // get char
	beq end		// Text end?
	jsr BSOUT	//print char
	iny		// index next char
	jmp t0		//loop
end:	
	rts		// bye
			
// https://doitwireless.com/2014/06/26/8-bit-pseudo-random-number-generator/
//0x8E, 0x95, 0x96, 0xA6, 0xAF, 0xB1, 0xB2, 0xB4, 0xB8, 0xC3, 0xC6, 0xD4, 0xE1, 0xE7, 0xF3, 0xFA	  	

.import source "tables.asm"
consoleCharSet:
	.const TILES_TEMPLATE = "Tiles=$00, PreLastLine=$b0, LastLine=$b8, EndTiles=$c0"
	.var tiles = LoadBinary("resources/menutileset.bin", TILES_TEMPLATE)
	.fill tiles.getTilesSize(), tiles.getTiles(i) + METAL_CHARS_START
	.fill tiles.getPreLastLineSize(), tiles.getPreLastLine(i) + METAL_CHARS_START
	.fill tiles.getPreLastLineSize(), tiles.getPreLastLine(i) + METAL_CHARS_START
	.fill tiles.getLastLineSize(), tiles.getLastLine(i) + METAL_CHARS_START 

bullitChars:
	 .import binary "resources/bullets.bin"
menuchars:	
     .import binary "resources/menuitems.bin"
metalchars:
	.import binary "resources/metalchars.bin"
sprites:	
	 .import binary "resources/sprites.bin"	

music:
	.import binary "resources/Chiptune7_1_sfx.bin"		
bitmap: 
.const AA_STUDIO_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2338, BackgroundColor = $2329, ForegroundColor = $2328"
.var picture = LoadBinary("resources/bitmap.ocp", AA_STUDIO_TEMPLATE)
screenRamSource:	
	.fill picture.getScreenRamSize(), picture.getScreenRam(i)
	.fill 24,0
colorSource:
	.fill picture.getColorRamSize(), picture.getColorRam(i)
	.fill 24,0
bitmapSource:	
	.fill picture.getBitmapSize(), picture.getBitmap(i)
charSource:	
	.import binary "resources/charset3_trimmed.bin"	