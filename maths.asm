//http://codebase64.org/doku.php?id=base:6502_6510_maths

.var multiplier	= MATHS_TEMP_ZP 
.var multiplicand = MATHS_TEMP_ZP + 2 
.var product = MATHS_TEMP_ZP + 4 
 
mult16: 
 		lda	#$00
		sta	product+2	; clear upper bits of product
		sta	product+3 
		ldx	#$10		; set binary count to 16 
shift_r:		
		lsr	multiplier+1	; divide multiplier by 2 
		ror	multiplier
		bcc	rotate_r 
		lda	product+2	; get upper half of product and add multiplicand
		clc
		adc	multiplicand
		sta	product+2
		lda	product+3 
		adc	multiplicand+1
rotate_r:	
		ror			; rotate partial product 
		sta	product+3 
		ror	product+2
		ror	product+1 
		ror	product 
		dex
		bne	shift_r 
		rts
		
        // prints a 32 bit value to the screen
printdec32:
        jsr hex2dec

        ldx #9
l1:      lda result,x
        bne l2
        dex             ; skip leading zeros
        bne l1

l2:      lda result,x
        ora #$30
        jsr $ffd2
        dex
        bpl l2
        rts

        // converts 10 digits (32 bit values have max. 10 decimal digits)
hex2dec:
        ldx #0
l3:      jsr div10
        sta result,x
        inx
        cpx #10
        bne l3
        rts

        // divides a 32 bit value by 10
        // remainder is returned in akku
div10:
        ldy #32         ; 32 bits
        lda #0
        clc
l4:      rol
        cmp #10
        bcc skip
        sbc #10
skip:   rol value
        rol value+1
        rol value+2
        rol value+3
        dey
        bpl l4
        rts

value:   .byte $ff,$ff,$ff,$ff

result:  .byte 0,0,0,0,0,0,0,0,0,0

// assumes value in A, x and y are indexes
.macro printdec(position)
{
		ldy #$2f
		ldx #$3a
		sec
!:		iny
		sbc #100
		bcs !-
!:		dex
		adc #10
		bmi !-
		adc #$2f
		sta position + 2
		stx position + 1
		sty position  
}		