/* ===============================================================
				MENU
================================================================*/				

.const MENU_TEXT_LINE_0 = 0
.const BITMAP_LINE = 82
.const MENU_LINE = 164

pauseTime:	.byte  0 

initMenu:			
{			
	
	lda #0
	sta shield
	sta shieldsDiv
	sta credits
	sta bullits
	sta bullitsDiv
	sta doStartGame
	lda #9
	sta lives

	sei

	ldx #0
!:	
	lda #WHITE	
	sta $d800, x
	sta $d800+250,x
	sta $d800+500,x
	sta $d800+750,x
	inx 
	cpx #250
	bne !-
	
	jsr writeNumbers

// video off	
	lda $d016
	ora #%00100000
	sta $d016

	
	//set char set at 4*1024 = $1000 + VIC START	
	lda $d018	
	and #%11110000	
	ora #%00000100	
	sta $d018
	// video matrix at $0400 + VIC START

	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00

	
	lda #BLACK	
	sta $d020	
	sta $d021	
	lda #IN_GAME_MC_1
	sta $d022	
	lda #IN_GAME_MC_2	
	sta $d023	
	
	lda #0
	sta doPrint
	sta $d015

	ldx #0
!:			

	jsr clearBitmapAndCharColor

	generateCopyCode("catback")		
	generateCopyCode("bitsnbobs")		

	ldx #0
!:	lda menuScreenData,x
	sta SCREEN_START+18*40,x
	lda menuScreenData+160,x
	sta SCREEN_START+22*40,x
	lda #WHITE	
	sta $d800, x
	sta $d800 + 18*40, x
	sta $d800 + 22*40, x
	
	lda menuTopColor,x
	sta $d800,x

	lda menuTopData,x
	sta SCREEN_START,x
	 
	inx
	cpx #160
	bne !-

	lda #0 
	sta menuTopTextCounter


// video on
	lda $d016
	and #%11011111
	sta $d016

		
/*======================================================			
			SETUP IRQ			
====================================================*/

	lda #0 
	sta $d012			// raster interrupt at  line 200										
	lda #27
	sta $d011										

	lda #<menuIRQTop
	sta $fffe
	lda #>menuIRQTop
	sta $ffff
	lda #<freeze
	sta $fffa
	lda #>freeze
	sta $fffb

    lda #$01										
    sta $d01a			// enable raster interrupt 

	cli
	
	rts


}			

menuTopTextCounter:			.byte 1
menuTopTextPauseCounter:	.byte 0
menuTopLo:	.byte <menuTopData, <menuTopData+160, <menuTopData+320, <menuTopData+480
menuTopHi:	.byte >menuTopData, >menuTopData+160, >menuTopData+320, >menuTopData+480

menuTopData:
	.encoding "screencode_upper"
	//    "1234567890123456789012345678901234567890"
	.text "  I WANT TO EQUIP                       "
	.text "      MY SHIP                           "
	.text "  BUT I DON'T HAVE                      " 
	.text "    ANY MONEY                           "
	
	.text "                                        "
	.text "                           WELL,        "
	.text "                       WHAT DO YOU HAVE?"
	.text "                                        "

	.text "                                        "
	.text "  EHM,                                  " 
	.text "  I'VE GOT 9 LIVES                      "
	.text "                                        "	
		
	.text "                       OK, I'LL GIVE YOU"
	.text "                        A CREDIT WORTH  "
	.text "                       FOR EACH LIFE YOU"
	.text "                         YOU CAN SPARE  "

menuTopColor:
	.fill 20, LIGHT_RED
	.fill 20, LIGHT_GREY
	.fill 20, LIGHT_RED
	.fill 20, LIGHT_GREY
	.fill 20, LIGHT_RED
	.fill 20, LIGHT_GREY
	.fill 20, LIGHT_RED
	.fill 20, LIGHT_GREY


menuScreenData:
	.encoding "screencode_upper"
	//    "0123456789012345678901234567890123456789"
	.text "   LIVES              CREDITS           "
	.text "                                        "
	.text "   BULLITS                              "	
	.text "   SHIELDS                              "
	.text "                                        "
	.text "   UP DOWN TO SELECT ITEM               "
	.text "   LEFT RIGHT TO IN-DECREASE AMOUNT     "
	.text "                  PRESS FIRE TO START   "

menuDescriptions:
	.text "HOW MANY LIVES CAN ALIEN CAT DO WITHOUT?"
	.text "BULLITS CAN GO THROUGH MULTIPLE ENEMIES "
	.text "SHIELDS CONVERT DAMAGE INTO BULLITS     "  

.var livesStart = SCREEN_START + 18*40 + 12 
.var creditsStart= SCREEN_START + 19*40 - 10 

handleJoy:
{
	lax $dc01
	cmp debounce
	sta debounce
	bne !+
	jmp end
!:	
 	//clear indicator, will be redrawn later 
	ldy selected
 	lda selectStart,y
 	tay
 	lda #32
 	sta livesStart - 11,y
 	
 	ldy selected
checkleft:
	txa
	and #%00000100
	bne checkright
	cpy #0	//lives
	bne !+
	lda lives
	cmp #1
	beq !+
	dec lives
	inc credits
	jmp end
!: 	cpy #1	//bullits
	bne !+
	lda bullitsDiv
	beq !+

	sed
	sec
	lda bullitsDec + 1
	sbc #BULLITS_PER_CREDIT
	sta bullitsDec + 1
	lda bullitsDec
	sbc #$00
	sta bullitsDec
	cld 
	dec bullitsDiv

	inc credits
	jmp end
!:	cpy #2	//shields
	bne !+
	lda shieldsDiv
	beq !+

	sed
	sec
	lda shieldsDec + 1
	sbc #SHIELDS_PER_CREDIT
	sta shieldsDec + 1
	lda shieldsDec
	sbc #$00
	sta shieldsDec
	cld 
	dec shieldsDiv
	inc credits
!:
	jmp end
checkright:
	txa
	and #%00001000
	bne checkup
	cpy #0	//lives
	bne !+
	lda credits
	beq !+
	dec credits
	inc lives
	jmp end
!: 	cpy #1	//bullits
	bne !+
	lda credits
	beq !+
	dec credits
	
	sed
	clc
	lda bullitsDec + 1
	adc #BULLITS_PER_CREDIT
	sta bullitsDec + 1
	lda bullitsDec
	adc #$00
	sta bullitsDec
	cld 

	inc bullitsDiv
	jmp end
!:	cpy #2	//shields
	bne !+
	lda credits
	beq !+
	dec credits

	sed
	clc
	lda shieldsDec + 1
	adc #SHIELDS_PER_CREDIT
	sta shieldsDec + 1
	lda shieldsDec
	adc #$00
	sta shieldsDec
	cld 

	inc shieldsDiv
!:
	jmp end
checkup:
	txa
	and #%00000001
	bne checkdown
	
	ldx	selected	
	dex	
	bpl !+	
	ldx #2	
!:	stx selected	

	jmp end
checkdown:	
	txa
	and #%00000010
	bne checkButton

	ldx	selected	
	inx	
	cpx #3	
	bne !+	
	ldx #0	
!:	stx selected	
	jmp end
checkButton:	
	txa
	cmp prevJoyVal	//debounce
	sta prevJoyVal
	beq end

	and #%00010000
	bne end

	lda #1	
	sta doStartGame
	lda #0
	sta doPrint


end:
	jmp writeNumbers
}


writeNumbers:
{
	printMenuVars() 	
	ldx selected
	lda descripStart,x
	tay
	ldx #0
!:	lda menuDescriptions,y
	sta SCREEN_START + 22*40,x
	iny
	inx
	cpx #40
	bne !-
 	rts
}
	
shieldsDiv:	.byte 0	
bullitsDiv:	.byte 0	
selected:	.byte 0
selectStart:	.byte 0,80,120, 160
descripStart:	.byte 0,40,80
doStartGame:	.byte 0

menuIRQTop:
	enterIrq()

	ldx #>menuIRQBitmap
	ldy #<menuIRQBitmap
	stx $FFFF
	sty $FFFE
	lda #BITMAP_LINE
	sta $D012

	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00

	lda $d011
	and #%01011111	//bitmap off, make sure bit 9 of line is off
	sta $d011

	lda #%00010100
	sta $d018

	lda $d016
	and #%11101111	//mc off
	sta $d016

	jsr handleJoy
	lda doStartGame
	bne initGameStart
	sec
	rol $D019
	jmp quitIrq

initGameStart:
	ldx #$FF
	sta coolDownCount

	jsr initGameScreen
	jmp irq_end


menuIRQBottom:
	enterIrq()
	ldx #>menuIRQTop
	ldy #<menuIRQTop
	stx $FFFF
	sty $FFFE
	lda #MENU_TEXT_LINE_0
	sta $D012

	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00

	lda $d011
	and #%01011111	//bitmap off, make sure bit 9 of line is off
	sta $d011

	lda #%00010100
	sta $d018

	lda $d016
	and #%11101111	//mc off
	sta $d016


	jmp irq_end		// also plays music


menuIRQBitmap:
	enterIrq()
	inc $d020

	ldx #>menuIRQBottom
	ldy #<menuIRQBottom
	stx $FFFF
	sty $FFFE
	lda #MENU_LINE
	sta $D012
	lda $d011
	and #$7f
	sta $d011
	
	lda $dd00
	and #%11111100
	sta $dd00

	lda $d011
	and #%01111111	//next interrupt line bit 9=0
	ora #%00100000	//enable bitmap
	sta $d011

	lda $d016
//	and #%11101111	//mc off
	ora #%00010000	//mc on
	sta $d016

	lda #%11101000 //bit 3: bitmap at 2nd 8K, bit 4-7: second to last 1K of char
	sta $d018
	sec
	rol $D019
	
	lda menuTopTextPauseCounter
	clc
	adc #$01
	
	cmp #128
	bne notYetNewText
	ldx #0
	lda menuTopTextCounter
	clc
	adc #$01
	and #$03
	sta menuTopTextCounter
	tay
	lda menuTopHi,y
	sta smMenu + 2
	lda menuTopLo,y
	sta smMenu + 1
smMenu: 
	lda $4444,x
	sta SCREEN_START,x
	inx
	cpx #160
	bne smMenu
	lda #0
notYetNewText:	
	sta menuTopTextPauseCounter
	sec
	rol $D019
	jmp quitIrq

