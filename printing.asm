// ===============PRINTING
.const INGAME_DIST_POS = SCREEN_START + 15*40 + 32 + 2
.const INGAME_LIVES_POS = SCREEN_START + 3*40 + 32 + 4   
.const INGAME_BULLIT_POS = SCREEN_START + 7*40 + 32 + 3   
.const INGAME_SHIELD_POS = SCREEN_START + 11*40 + 32 + 3   

.const MENU_BULLIT_POS = SCREEN_START + 20*40 + 12   
.const MENU_SHIELD_POS = SCREEN_START + 21*40 + 12   

.const MENU_BULLIT_POS_RESTORE = STARS_CHARS_AT + 20*40 + 12   
.const MENU_SHIELD_POS_RETORE = STARS_CHARS_AT + 21*40 + 12   


.macro printVars(){
	lda doPrint
	bne ingamePrint
	jmp end

ingamePrint:	

	lda bullitsDec
	and #$0f
	adc #$30
	sta INGAME_BULLIT_POS
	lax bullitsDec + 1
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta INGAME_BULLIT_POS + 1 
	txa
	and #$0f
	adc #$30
	sta INGAME_BULLIT_POS + 2
	
	lda shieldsDec
	and #$0f
	adc #$30
	sta INGAME_SHIELD_POS
	lax shieldsDec + 1
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta INGAME_SHIELD_POS + 1 
	txa
	and #$0f
	clc
	adc #$30
	sta INGAME_SHIELD_POS + 2

	lax distanceToTravelDec
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta INGAME_DIST_POS
	txa
	and #$0f
	adc #$30
	sta INGAME_DIST_POS + 1
	lax distanceToTravelDec + 1
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta INGAME_DIST_POS + 2 
	txa
	and #$0f
	clc
	adc #$30
	sta INGAME_DIST_POS + 3

	lda lives
	clc
	adc #$30
	sta INGAME_LIVES_POS
end:
}

.macro printMenuVars(){
	ldx #0
loop:	
	cpx lives
	lda #32
	bcs !+
	lda #CAT_CHAR
!:	sta livesStart,x 
	cpx credits
	lda #32
	bcs !+
	lda #CREDIT_CHAR
!:	sta creditsStart,x 
 	inx
 	cpx #9
 	bne loop
 	
 	ldx selected
 	lda selectStart,x
 	tax
 	lda #31
 	sta livesStart - 11,x

	lda bullitsDec
	clc
	and #$0f
	adc #$30
	sta MENU_BULLIT_POS
	lax bullitsDec + 1
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta MENU_BULLIT_POS + 1 
	txa
	and #$0f
	adc #$30
	sta MENU_BULLIT_POS + 2
	
	lda shieldsDec
	and #$0f
	adc #$30
	sta MENU_SHIELD_POS
	lax shieldsDec + 1
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta MENU_SHIELD_POS + 1 
	txa
	and #$0f
	clc
	adc #$30
	sta MENU_SHIELD_POS + 2

end:

}