.segment "BHDR"
                .word   $0801
                .word   $080b 
                .word   2018 
                .byte   $9e 
                .byte   "2061",$0,$0,$0

buf1r		= $55
buf1		= $56
buf1t		= $5c
buf2		= $5d

.data
lines:		.byte	$ed
pages:		.byte	$8

.code
		sei
		lsr	$1
		ldy	#$0
		sty	$fe
		sty	buf1r
		sty	buf1t
loop:		ldx	#$6
rdpage		= *+2
rdloop:		lda	$d000,y
		iny
		ror	lines
		bcc	rdloop
		and	#$7e
		sta	buf1r,x
		dex
		bne	rdloop
		ror	lines
		sty	$fd
		stx	buf2+6
		stx	buf2+7
		ldx	#$5
olloop:		lda	buf1,x
		sta	buf2,x
		ora	buf2+2,x
		sta	buf2+2,x
		lda	buf1,x
		asl	a
		ora	buf2+1,x
		sta	buf2+1,x
		lda	buf1,x
		lsr	a
		ora	buf2+1,x
		sta	buf2+1,x
		dex
		bpl	olloop
		ldy	$fe
		ldx	#$7
wrloop:		lda	buf2,x
		eor	buf1r,x
wrpage		= *+2
		sta	$3800,y
		iny
		dex
		bpl	wrloop
		sty	$fe
		ldy	$fd
		bne	loop
		inc	wrpage
		inc	rdpage
		dec	pages
		bne	loop
		sec
		rol	$1
		lda	#$1e
		sta	$d018
		cli
		rts

