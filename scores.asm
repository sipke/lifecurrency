.const NO_SCORE = 40
/*
		PRINT LINE
.const ZPTMP1 = $33
.const ZPTMP2 = $34
		TEXT SOURCE
.const ZPTMP3 = $35
.const ZPTMP4 = $36

//temp score
.const ZPTMP5 = $37
.const ZPTMP6 = $38
.const ZPTMP7 = $39
.const ZPTMP8 = $3a

*/
.var descr =List()
.eval descr.add("YAPPER KILLED@")
.eval descr.add("FRANKFURTERS KILLED@")
.eval descr.add("BELLOWS KILLED@")
.eval descr.add("YELLOW BELLOWS KILLED@")
.eval descr.add("PURPLE FRANKFURTERS KILLED@")
.eval descr.add("SPIKES KILLED@")
.for (var i=6;i<16;i++) {
	.eval descr.add("BUTCH" + i + "S KILLED@")
}
.var LIVES_INDEX = descr.size()
.eval descr.add("LIVES LEFT BONUS@")
.var BULLITS_INDEX = descr.size()
.eval descr.add("BULLITS LEFT BONUS@")
.var SHIELDS_INDEX = descr.size()
.eval descr.add("SHIELDS LEFT BONUS@")
.var CREDITS_INDEX = descr.size()
.eval descr.add("CREDITS LEFT BONUS@")

.var TOTALS_INDEX = descr.size()
.eval descr.add("TOTAL SCORE@")

.var HIGHSCORE_INDEX = descr.size()
.eval descr.add("HIGH SCORE !!@")


.encoding "screencode_upper"
descrips:
.for (var i=0; i<descr.size();i++){
	.text descr.get(i)
}
.var p = descrips
descrPointLo:
.for (var i=0; i<descr.size();i++){
	.byte <p
	.eval p+= descr.get(i).size()
}
.eval p = descrips
descrPointHi:
.for (var i=0; i<descr.size();i++){
	.byte >p
	.eval p+= descr.get(i).size()
}

printTotals:
{
//empty line
	lda ZPTMP1
	clc
	adc #$28
	sta ZPTMP1
	lda ZPTMP2
	adc #$00
	sta ZPTMP2

	ldy #40-8
	ldx #7

	.for (var i=4; i>=0; i--){
	lda score + 3 + i
	and #$0f
	clc
	adc #$30
	sta (ZPTMP1),y
	sta highscoretmp,x
	dey
	dex
	lda score + i
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta (ZPTMP1),y
	sta highscoretmp,x
	dey
	dex

	}

	ldy #TOTALS_INDEX
	lda descrPointLo,y
	sta ZPTMP3
	lda descrPointHi,y
	sta ZPTMP4
	
	ldy #0
!:	lda (ZPTMP3),y
	beq donePrinting		//wait for '@'
	sta (ZPTMP1),y
	iny
	jmp !-
donePrinting:

	lda ZPTMP1
	clc
	adc #$28
	sta ZPTMP1
	lda ZPTMP2
	adc #$00
	sta ZPTMP2


	rts
}
printScoreLine: {
	ldy #40-8

	.for (var i=0; i<4; i++){
	lax ZPTMP5 + i
	and #$0f
	clc
	adc #$30
	sta (ZPTMP1),y
	dey
	txa
	lsr
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta (ZPTMP1),y
	dey

	}
	
	ldy #0
!:	lda (ZPTMP3),y
	beq donePrinting		//wait for '@'
	sta (ZPTMP1),y
	iny
	jmp !-
donePrinting:
	lda ZPTMP1
	clc
	adc #$28
	sta ZPTMP1
	lda ZPTMP2
	adc #$00
	sta ZPTMP2
	
	rts
}

initTmpScore: {
	lda #0
	sta ZPTMP5
	sta ZPTMP6
	sta ZPTMP7
	sta ZPTMP8
	rts
}
repeatScore:	.byte 0
//x is number of repeats
//y is index of description
// a score per repeat
addRepeatScore: 
	sta repeatScore	
	jsr initTmpScore
		sed
!:		lda ZPTMP5
		clc 
		adc repeatScore
		sta ZPTMP5

		lda ZPTMP6
		adc #0
		sta ZPTMP6

		lda ZPTMP7
		adc #0
		sta ZPTMP7

		lda ZPTMP8
		adc #0
		sta ZPTMP8

		dex
		bne !-
		cld
		lda descrPointLo,y
		sta ZPTMP3
		lda descrPointHi,y
		sta ZPTMP4
		jsr printScoreLine

	sed
		lda score + 7
		clc
		adc ZPTMP5
		sta score + 7

		lda score + 6
		adc ZPTMP6
		sta score + 6

		lda score + 5
		adc ZPTMP7
		sta score + 5

		lda score + 4
		adc ZPTMP6
		sta score + 4

		lda score + 3
		adc #0
		sta score + 3

	cld
	rts

scoresPerEnemyType: {
	//use y as indication of none zero score
	.for (var i=0; i<16; i++) {
		ldy #i
		lda scorePerType + i
		ldx killsPerType + i
		beq !next+
		jsr addRepeatScore
!next: 				
	}
	rts
}

scoresForStuffLeft:
		jsr initTmpScore
		

		sed
		lda livesAtStart
		sec
		sbc lives
		tax
		ldy #LIVES_INDEX
		lda #POINTS_PER_LIFE_LEFT
		jsr addRepeatScore

		sed
		lda bullitsAtStart
		sec
		sbc bullitsDec
		tax
		ldy #BULLITS_INDEX
		lda #POINTS_PER_BULLIT_LEFT
		jsr addRepeatScore
		cld
		
		sed
		lda shieldsAtStart
		sec
		sbc shieldsDec
		tax
		ldy #SHIELDS_INDEX
		lda #POINTS_PER_SHIELD_LEFT		
		jsr addRepeatScore
		cld 
		
		ldx credits
		ldy #CREDITS_INDEX
		lda #POINTS_PER_CREDIT_LEFT
		jsr addRepeatScore
		
		rts

/*
calcNTSC:
break()
	lda NTSC
	beq !+
	sed
	lda achievementScore + 5	
	clc 	
	adc #23	
	sta achievementScore + 5	
	lda achievementScore + 4	
	adc #01	
	sta achievementScore + 4	
	lda achievementScore + 3	
	adc #00	
	sta achievementScore + 3	
	lda achievementScore + 2	
	adc #00	
	sta achievementScore + 2	
	lda achievementScore + 1	
	adc #00	
	sta achievementScore + 1	
	lda achievementScore + 0	
	adc #00	
	sta achievementScore + 0	
!:
	rts
*/
scoreLine:	.byte 0
scoresdone:	.byte 0
countdownafterscores:	.byte $ff
scoreHandling:
	lda printingScores
	bne printScores
	lda scoresdone
	beq bye
	lda countdownafterscores
	bne !+
	lda $dc01
	and #%00010000
	 
!:	
	dec countdownafterscores
bye:	
	rts

printScores: {
	lda #0
	sta doPrint
	sta printingScores
	sta scoresdone
	
	ldx #0
!:	lda #0	
	sta score,x
	clc
	adc #$30
	sta highscoretmp,x
	inx
	cpx #8
	bne !-
	
	ldx #0
!:	lda printScoresText,x
	sta SCREEN_START,x
	inx
	cpx #40
	bne !-
	lda #<SCREEN_START + 40
	sta ZPTMP1
	lda #>SCREEN_START + 40
	sta ZPTMP2

	jsr scoresPerEnemyType
	jsr scoresForStuffLeft
	jsr printTotals
	jsr compareForHighScore
	rts
}
compareForHighScore: {
break()
	ldx #0
!:	lda highscoretmp,x
	cmp HIGH_SCORE_TEXT_LOCATION,x
	bcc nohighscore
	inx
	cpx #8
	bne !-
highscore:
	lda ZPTMP1
	clc
	adc #$28
	sta ZPTMP1
	lda ZPTMP2
	adc #$00
	sta ZPTMP2
	ldy #HIGHSCORE_INDEX
	lda descrPointLo,y
	sta ZPTMP3
	lda descrPointHi,y
	sta ZPTMP4

	ldy #0
!:	lda (ZPTMP3),y
	beq donePrinting		//wait for '@'
	sta (ZPTMP1),y
	iny
	jmp !-
donePrinting:
	ldx #0
!:	lda highscoretmp,x
	sta HIGH_SCORE_TEXT_LOCATION,x
	inx
	cpx #16
	bne !-
nohighscore:	
	lda #1
	sta scoresdone
	rts
	
	
}

//                         012345678901234567890012345678901234567890
.encoding "screencode_upper"
printScoresText:	.text "                     SCORES               "
	