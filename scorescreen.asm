setupScoreScreen:

	sei

	ldx #0
	
!:			

	lda #WHITE			
	sta $d800, x
	sta $d800+250,x
	sta $d800+500,x
	sta $d800+750,x
	lda #32
	sta SCREEN_START,x
	sta SCREEN_START+250,x
	sta SCREEN_START+500,x
	sta SCREEN_START+750,x
	inx 
	cpx #250
	bne !-

	lda #<sc_irq_top
	sta $fffe
	lda #>sc_irq_top
	sta $ffff
	lda #<freeze
	sta $fffa
	lda #>freeze
	sta $fffb
	
	lda #00 
	sta $d012			// raster interrupt at  line 										
											

	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00

	lda $d011
	and #%01011111	//bitmap off, make sure bit 9 of line is off
	sta $d011

	lda #%00010100
	sta $d018

	lda $d016
	and #%11101111	//mc off
	sta $d016
										
	ldx #1										
	stx printingScores										
											
	jmp irq_end 

initIntro3:
	lda #$2F
	sta coolDownCount

	jsr setupStartScreen //initMenu //initGameScreen
	jmp irq_end


sc_irq_top:
	enterIrq()
	jsr MUSIC_START + 3	
		
	lda scoresdone
	beq scoresNotYetDone

	lda $dc01
	and #$10
	beq initIntro3
scoresNotYetDone:	
	sec
	rol $D019
	jmp quitIrq
