.struct BitmapSourceTarget {width, height, sourceX, sourceY, targetX, targetY}

.var bitmapsourceTargets = Hashtable()
	.eval bitmapsourceTargets.put("catstart", BitmapSourceTarget(7,10,0,6,16,6))
	.eval bitmapsourceTargets.put("alien", BitmapSourceTarget(10,2,6,0,4,2))
	.eval bitmapsourceTargets.put("easter", BitmapSourceTarget(12,2,16,0,15,2))
	.eval bitmapsourceTargets.put("cat", BitmapSourceTarget(6,2,28,0,28,2))
	
	.eval bitmapsourceTargets.put("helmet", BitmapSourceTarget(6,5,0,0,16,4))
	.eval bitmapsourceTargets.put("game", BitmapSourceTarget(9,2,7,2,10,11))
	.eval bitmapsourceTargets.put("over", BitmapSourceTarget(9,2,16,2,20,11))

	.eval bitmapsourceTargets.put("transfer", BitmapSourceTarget(15,9,9,7,11,7))
	.eval bitmapsourceTargets.put("youve", BitmapSourceTarget(11,2,7,4,6,2))
	.eval bitmapsourceTargets.put("done", BitmapSourceTarget(9,2,18,4,18,2))
	.eval bitmapsourceTargets.put("it", BitmapSourceTarget(5,2,27,4,28,2))

	.eval bitmapsourceTargets.put("baby", BitmapSourceTarget(5,9,35,0,18,5))
	.eval bitmapsourceTargets.put("its", BitmapSourceTarget(8,2,26,9,11,16))
	.eval bitmapsourceTargets.put("a", BitmapSourceTarget(2,2,35,9,20,16))
	.eval bitmapsourceTargets.put("boy", BitmapSourceTarget(6,2,26,11,23,16))
	.eval bitmapsourceTargets.put("girl", BitmapSourceTarget(7,2,33,11,23,16))
	.eval bitmapsourceTargets.put("excl", BitmapSourceTarget(1,2,31,4,31,16))
	.eval bitmapsourceTargets.put("shell1", BitmapSourceTarget(2,2,33,6,16,13))
	.eval bitmapsourceTargets.put("shell2", BitmapSourceTarget(2,1,34,8,24,14))
	.eval bitmapsourceTargets.put("shell3", BitmapSourceTarget(1,1,33,8,27,13))

	.eval bitmapsourceTargets.put("catback", BitmapSourceTarget(6,7,8,18,9,7))
	.eval bitmapsourceTargets.put("bitsnbobs", BitmapSourceTarget(14,8,15,17,15,5))


.macro generateCopyCode(key) {
	.var bms = bitmapsourceTargets.get(key)

	lda #< (BITMAP_IN_MEM + 320 * bms.sourceY + 8* bms.sourceX)
	sta BITMAP_TEMP_ZP
	lda #>(BITMAP_IN_MEM + 320 * bms.sourceY + 8* bms.sourceX)
	sta BITMAP_TEMP_ZP + 1
	lda #<(BITMAP_TARGET + 320 * bms.targetY + 8* bms.targetX)
	sta BITMAP_TEMP_ZP +2
	lda #>(BITMAP_TARGET + 320 * bms.targetY + 8* bms.targetX)
	sta BITMAP_TEMP_ZP + 3

	ldx #bms.width
	ldy #bms.height

	jsr copyBitmapPart
//=====================
	lda #<(COLOR_DATA + 40 * bms.sourceY + bms.sourceX)
	sta BITMAP_TEMP_ZP
	lda #>(COLOR_DATA + 40 * bms.sourceY + bms.sourceX)
	sta BITMAP_TEMP_ZP + 1
	lda #<($d800 + 40 * bms.targetY + bms.targetX)
	sta BITMAP_TEMP_ZP +2
	lda #>($d800 + 40 * bms.targetY + bms.targetX)
	sta BITMAP_TEMP_ZP + 3

	lda #<(SCREEN_RAM_DATA + 40 * bms.sourceY + bms.sourceX) 
	sta BITMAP_TEMP_ZP + 4
	lda #>(SCREEN_RAM_DATA + 40 * bms.sourceY + bms.sourceX)
	sta BITMAP_TEMP_ZP + 5
	lda #<(CHAR_DATA_TARGET + 40 * bms.targetY + bms.targetX)
	sta BITMAP_TEMP_ZP +6
	lda #>(CHAR_DATA_TARGET + 40 * bms.targetY + bms.targetX)
	sta BITMAP_TEMP_ZP + 7

	ldx #bms.width
	ldy #bms.height
	
	jsr copyColorPart	



}
	