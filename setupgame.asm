setupSprites:
	lda #PLAYER_SPRITE
	sta 2040 + VIC_START

	lda #EXHAUST_SPRITE
	sta SPRITE_POINTERS + 1
	
	lda #ENEMY_SPRITE_1
	sta 2042 + VIC_START
	sta 2043 + VIC_START
	sta 2044 + VIC_START
	sta 2045 + VIC_START
	sta 2046 + VIC_START

	lda #MISSILE_SPRITE
	sta 2047 + VIC_START

	//MSB
	lda #%00000000
	sta $d010
	//multicolor
	lda #%01111110
	sta $d01c
	
	ldx #7
!:	lda spriteColors,x
	sta $d027,x
	dex
	bpl !-
	
//MC COLORS	
	lda #BROWN
	sta $d025
	lda #LIGHT_GREY
	sta $d026
	
//expand	none
	lda #%00000000
	sta $d01d
	sta $d017

	//enable
	lda #%00000011
	sta $d015

	lda playerStartPosX
	sta $d000
	sta $d002
	asl
	sta POSXLO
	bcc !+
	inc POSXHI
!:	lda playerStartPosY
	sta $d001
	sta $d003
	asl
	sta POSYLO
	bcc !+
	inc POSYHI
!:

	lda #0
	sta score	
	sta score + 1	
	sta score + 2	
	sta score + 3	
	sta score + 4	
	sta score + 5		
	sta score + 6			
	sta score + 7				
	sta score + 8					

	lda #$ff
	sta countdownafterscores

	lda bullitsDec
	sta bullitsAtStart		
	lda shieldsDec
	sta shieldsAtStart		
	lda lives		
	sta livesAtStart		
		
		
	rts
	
initVars:
	ldx #0
	lda #0
!:	sta MOVEMENT_VARS_START,x
	inx
	bne !-
	
	ldx #5
!:	sta enemyAlive,x
	dex
	bpl !-
	
	sta playerDying
	
	lda #2
	sta starfieldSpeed
	
	lda #1
	sta doPrint

	lda #>DIST_TO_TRAVEL
	sta distanceToTravelDec
	lda #<DIST_TO_TRAVEL
	sta distanceToTravelDec +1
	
	rts

initGameScreen:
	sei

	jsr initVars
	jsr setupSprites

	jsr copyStarField
	jsr initStarFieldChars

	// multicolor
	lda $d016
	ora #%00010000
	sta $d016

	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00

	lda $d011
	and #%01011111	//bitmap off, make sure bit 9 of line is off
	sta $d011

	lda #%00010100
	sta $d018

	lda $d016
	and #%11101111	//mc off
	sta $d016
	
// build gamescreen
	lda #BLACK	
	sta $d020	
	sta $d021	
	lda #BLACK	
	sta $d022	
	lda #DARK_GREY	
	sta $d023	


	lda #<$d800
	sta sm0+1
	lda #>$d800
	sta sm0+2
	lda #<$0400+VIC_START
	sta sm1+1
	lda #>$0400+VIC_START
	sta sm1+2

	lda #<consoleCharSet//consoleChars
	sta sm2+1
	lda #>consoleCharSet//consoleChars
	sta sm2+2


//break()
	ldy #25 
l1:	ldx #32
l2: lda #METAL_COLOR + 8
sm0:	
	sta $1111,x
sm2:	
	lda consoleCharSet//consoleChars
sm1:	
	sta $2222,x
	inc sm2 +1
	bne !+
	inc sm2 +2
!:	inx
	cpx #40
	bne l2

	lda sm0+1
	clc
	adc #$28
	sta sm0+1
	lda sm0+2
	adc #$00
	sta sm0+2

	lda sm1+1
	clc
	adc #$28
	sta sm1+1
	lda sm1+2
	adc #$00
	sta sm1+2

	dey
	bne l1

	lda #BULLIT_CHAR	
	sta INGAME_BULLIT_POS - 39	
	lda #SHIELD_CHAR	
	sta INGAME_SHIELD_POS - 39	
	lda #CAT_CHAR	
	sta INGAME_LIVES_POS - 40	
	lda #DIST_LEFT_CHAR	
	sta INGAME_DIST_POS - 39	
	lda #DIST_RIGHT_CHAR	
	sta INGAME_DIST_POS - 38	
	

//init  game vars
	lda #0
	sta blinking
	sta score
	sta shieldActive
	sta playerDying

	sta isFired	
		
	sta level
	sta curEnemy
	sta freeBullit
	
	lda #2
	sta starfieldSpeed
	sta freeEnemy
	
	lda #>DIST_TO_TRAVEL
	sta distanceToTravelDec
	lda #<DIST_TO_TRAVEL
	sta distanceToTravelDec +1

	
	ldx #0
!:  lda #0	
	sta enemyAlive,x	
	sta enemyType,x
	sta efc,x
	sta edx,x
	sta edy,x
	sta enemyAlive,x
	sta enemyDying,x
	sta enemyPatternCounter,x
	sta enemyFramesTillNextPattern,x
	sta enemyPattern,x
 	sta enemyType,x

	inx
	cpx #8
	bne !-
	
	ldx #0
	lda #0
!:	sta	killsPerType,x
	inx
	cpx #$10
	bne !-
	
	ldx #MAX_NR_BULLITS
	lda #0
!:	sta bullitActive,x
	dex
	bne !-
	
	jsr setupIrq
	cli
	
	lda #CURRENT_WAVE_START_VAL
	sta currentWave
	
	lda #NR_FRAMES_BEFORE_FIRST_WAVE
	sta countDown
	
	lda #0
	sta gameEnded
	rts

setupIrq:
	lda #<irq
	sta $fffe
	lda #>irq
	sta $ffff
	lda #<freeze
	sta $fffa
	lda #>freeze
	sta $fffb
	lda #IRQ_RASTER
	sta $d012			// raster interrupt at  line 200										
	lda #27
	sta $d011			// irq line bit 9 =0							
									
    lda #$01										
    sta $d01a			// enable raster interrupt 
	rts

