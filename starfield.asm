createStarField:
{

// create first row of random start chars 
	ldx #0

!:	getRandom()

	and #$0f
	clc
	adc #STAR_CHARS_START
	sta STARS_CHARS_AT,x
	inx 
	cpx #40
	bne !-

	ldy #0
// next rows are generated by adding one to the value above
loop0:	
	ldx #0	
loop1:		
	lda STARS_CHARS_AT,x		
	clc		
	adc #$01		
	and #$0f
	adc #STAR_CHARS_START 
sm0:			
	sta STARS_CHARS_AT + 40,x		
	inx
	cpx #40
	bne loop1

	lda loop1 +1
	clc
	adc #40
	sta loop1 +1
	lda loop1 +2
	adc #$00
	sta loop1 +2

	lda sm0 +1
	clc
	adc #40
	sta sm0 +1
	lda sm0 +2
	adc #$00
	sta sm0 +2
	
	iny
	cpy #24
	bne loop0

	ldx #0
	lda #YELLOW
!:	
	getRandom()	
	sta STARS_COL_AT,x
	getRandom()
	sta STARS_COL_AT+250,x
	getRandom()
	sta STARS_COL_AT+500,x
	getRandom()
	sta STARS_COL_AT+750,x
	inx
	cpx #250
	bne !-


	rts
}
initStarFieldChars:
// clear chars again
	lda #0
	ldx #127
!:	sta FIRST_STAR_CHAR_BYTE,x
	dex
	bpl !-


// init downward moving dot
	ldx #0 
	stx currentStarFieldCharLine
	lda #8
	sta FIRST_STAR_CHAR_BYTE
rts

/* routine must be repeatable */
copyStarField:	
{

	//from
	lda #<STARS_CHARS_AT
	sta ZPTMP1
	lda #>STARS_CHARS_AT
	sta ZPTMP1+ 1
	//to
	lda #<SCREEN_START
	sta ZPTMP1 + 2
	lda #>SCREEN_START
	sta ZPTMP1 + 3

	ldy #0
loop0:	
	lda (ZPTMP1),y
	sta (ZPTMP1+2),y
	iny
	bne !+
	inc ZPTMP1 +1		//assume both aligned
	inc ZPTMP1 +3
	jmp loop0
!:	cpy #$e8
	bne loop0
	lda ZPTMP1 +1
	cmp #>STARS_CHARS_AT+1000
	bne loop0
	
// same for colors
	//from
	lda #<STARS_COL_AT
	sta ZPTMP1
	lda #>STARS_COL_AT
	sta ZPTMP1+ 1
	//to
	lda #$00
	sta ZPTMP1 + 2
	lda #$D8
	sta ZPTMP1 + 3

	ldy #0
loop1:	
	lda (ZPTMP1),y
	sta (ZPTMP1+2),y
	iny
	bne !+
	inc ZPTMP1 +1		//assume both aligned
	inc ZPTMP1 +3
	jmp loop1
!:	cpy #$e8
	bne loop1
	lda ZPTMP1 +1
	cmp #>STARS_COL_AT+1000
	bne loop1

	rts	
}

.macro updateChars(){
	lda starfieldSpeed
	beq end
	cmp #2
	beq speed2
speed1:
	ldx currentStarFieldCharLine
	cpx #127
	beq !+
	lda #8
	sta FIRST_STAR_CHAR_BYTE + 2,x
	lda #0
	sta FIRST_STAR_CHAR_BYTE,x
	inx
	stx currentStarFieldCharLine
	jmp end	
!: 	lda #8
	sta FIRST_STAR_CHAR_BYTE
	lda #0
	sta FIRST_STAR_CHAR_BYTE,x
	sta currentStarFieldCharLine
	jmp end
speed2:
	ldx currentStarFieldCharLine
	cpx #126
	beq !+
	lda #8
	sta FIRST_STAR_CHAR_BYTE + 2,x
	lda #0
	sta FIRST_STAR_CHAR_BYTE,x
	inx
	inx
	stx currentStarFieldCharLine
	jmp end	
!: 	lda #8
	sta FIRST_STAR_CHAR_BYTE
	lda #0
	sta FIRST_STAR_CHAR_BYTE,x
	sta currentStarFieldCharLine
	
	end:
}
