irq_pointer: .word 0

prevJoyVal:	.byte 0

bmsbx: .byte 0
blsbx: .byte 0
bmsby: .byte 0
bchar: .byte 0

//  bullitvars ====================================
bullitActive:	.fill MAX_NR_BULLITS,0
nrbullits: .byte MAX_NR_BULLITS -1
bullitX: .fill MAX_NR_BULLITS, random()*128
bullitY: .fill MAX_NR_BULLITS, random()*100 

bullitCharXTmp:	.byte 0
bullitCharYTmp:	.byte 0
bullitHits:		.byte 0

bullitSpeedX: .fill MAX_NR_BULLITS, 0// random()*6-3
bullitSpeedY: .fill MAX_NR_BULLITS, 0 //random()*6-3

prevPosHi: .fill MAX_NR_BULLITS,>$0400+VIC_START
prevPosLo: .fill MAX_NR_BULLITS,<$0400+VIC_START

prevChar: .fill MAX_NR_BULLITS, $20	
prevCol: .fill MAX_NR_BULLITS, LIGHT_GREY

curBullit:	.byte 0

bullitDrawOffsetLo:
	.fill 25, < SCREEN_START +   i*40
bullitDrawOffsetHi:
	.fill 25, > SCREEN_START +  i*40

tmpCharByte:	.byte


//---------------------------------------------
//			MISSILE
isFired:	.byte 0
laserX:		.byte 0
laserY:		.byte 0

// ---------------------------------------- 
//					sprite data 
//------------------------------------------------ 
playerStartPosX: 	.byte PLAYER_START_X
playerStartPosY: 	.byte PLAYER_START_Y
playerCharPosX1:	 .byte floor((PLAYER_START_X-24)/8)
playerCharPosX2:	 .byte floor((PLAYER_START_X-24)/8) + 1
playerCharPosY1:	 .byte floor((PLAYER_START_Y-50)/8)
playerCharPosY2:	 .byte floor((PLAYER_START_Y-50)/8)+1

playerDying:		.byte 0

shield:			.byte 0
shieldActive: 	.byte 0
lives:			.byte 0
bullits: 		.byte 0
credits:		.byte 0
score:			.fill 8,0		// for max of 16 digit decimal score
 //  for comparison high score
 .encoding "screencode_upper"
highscoretmp:		.fill 8, '0'		// contains digit chars of high score


// decimal mode
bullitsDec:		.word 0
shieldsDec:		.word 0		

bullitsAtStart: .word 0		
shieldsAtStart:	.word 0		
livesAtStart:	.word 0		

distanceToTravelDec:	.byte >DIST_TO_TRAVEL, <DIST_TO_TRAVEL 

// for printing
calculatingScores:	.byte 0
printingScores:		.byte 0
doPrint: 	.byte 0
//------------------------

blinking:	.byte 0
blinkingCount:	.byte 0

// enemy dx from pattern
edx:			.fill 8,0
// enemy dy from pattern
edy:			.fill 8,0
// enemy frame count till next change in pattern
efc:			.fill 8,0
//alive: do movement and stuff
enemyAlive:		.fill 8,0
//not alive but dying: do animation
enemyDying:		.fill 8,0
//to indicate it is not present in current wave
//a dirty fix, it prevents  enemies being handled while they are not in in wave 
enemyInWave: .fill 8,0

enemyPatternCounter: .fill 8, 0 //floor(random()*32)
enemyFramesTillNextPattern: .fill 8, 0 // floor(random()*32)
enemyPattern:	.fill 8, 0
enemyType:	.fill 8,0

newWave:	.byte 0
currentWave:	.byte CURRENT_WAVE_START_VAL


toggler:	.byte 0

animationFrame:			.fill 8,0

// counters that keep track how many frames have passed since last update, noit every frame those are increased or decreased
animationCounter:			.byte 0
shieldDecreaseCounter:		.byte 0
distanceDecreaseCounter: .byte 0

bullitChance: .fill 8,0

freeEnemy: .byte 0
freeBullit: .byte 0

curEnemy:	.byte 0

countDown:	.byte NR_FRAMES_BEFORE_FIRST_WAVE

.import source "enemyTypes.asm"		
.import source "waveData.asm"	


level:		.byte 0
spriteColors: 
	.byte WHITE, YELLOW, 10, 10, 10,10,10,10,10
patternDelay:	.fill 8,0

startPosXOffSet:
	.fill 8, 60
startPosYOffSet:
	.fill 8, 30

// -----------------------------------------------------------------
randomSeed: .byte $18 //floor(random()*256)
coolDownCount:	.byte 0
debounce: .byte 0

irqDone:	.byte 0
//---------------------------------------------------------------

gameEnded: 	.byte 0;

orBit: .fill 8, pow(2,i)
andBit: .fill 8, 255-pow(2,i)
missileEnemyBitAndTable: .fill 8, (128 | pow(2,i)) 
sfx1:
        .byte $00,$F1,$00,$CC,$41,$CC,$21,$CC,$11,$CC,$41,$CC,$21,$C0,$11,$C0
        .byte $41,$B4,$21,$B4,$11,$B4,$20,$00
sfx2:
        .byte $00,$FC,$00,$A4,$81,$A4,$80,$A4,$A4,$A4,$00
currentStarFieldCharLine:	.byte 0
starfieldSpeed:				.byte 2
