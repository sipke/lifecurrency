.const BITMAP_IN_MEM = $c000 //8000 bytes of bitmap
.const COLOR_DATA = $A000	// 
.const SCREEN_RAM_DATA = $A400	

.const FILLED_SPRITE_START = $E000 - 64

.const CHAR_DATA_TARGET = $f800
.const BITMAP_TARGET = $e000

.var BSOUT  = $f1ca //$ffd2	// print value in accu on screen
.var TXTOUT_LB = $22	// pointer for text lb address (unused memory on zeropage)
.var TXTOUT_HB = $23	// pointer for text hb address (unused memory on zeropage)

.const IRQ_RASTER = 238

.const VIC_START =$4000//
.const CHARS_AT = $1000 + VIC_START
.const SCREEN_START = $0400 + VIC_START

.const STARS_CHARS_AT  = $A800 // will save starfield and starfield colors here 
.const STARS_COL_AT = $AC00 

.const MUSIC_START = $b400
.const MUSIC_BLOCK_LENGTH = 12


.const LIVES_PRINT_POS = SCREEN_START + 1*40 + 32 + 4
.const SHIELD_PRINT_POS = SCREEN_START + 3*40 + 32 + 4    
.const BULLITS_PRINT_POS = SCREEN_START + 5*40 + 32 + 4    


/*========================= SPRITES =========================================*/
.const SPRITE_START = VIC_START + $2000

.const SPRITE_POINTER_START = (SPRITE_START- VIC_START)/64
//  alive animations are BASE eor #$01 
.const PLAYER_SPRITE = SPRITE_POINTER_START//
.const PLAYER_DYING_SPRITE = 24 + SPRITE_POINTER_START//

//death animations are all 4 long 
.const SHIELD_SPRITE = 2 + SPRITE_POINTER_START//
.const EXHAUST_SPRITE = 4+ SPRITE_POINTER_START//
.const ENEMY_SPRITE_1 = 8 +SPRITE_POINTER_START
.const ENEMY_SPRITE_1_DEATH = 12 +SPRITE_POINTER_START
.const ENEMY_SPRITE_2 = 16 +SPRITE_POINTER_START
.const ENEMY_SPRITE_2_DEATH = 20 +SPRITE_POINTER_START
.const ENEMY_SPRITE_3 = 32 +SPRITE_POINTER_START
.const ENEMY_SPRITE_3_DEATH = 28 +SPRITE_POINTER_START
.const ENEMY_SPRITE_4 = 34 +SPRITE_POINTER_START
.const ENEMY_SPRITE_4_DEATH = 36 +SPRITE_POINTER_START
//
.const MISSILE_SPRITE = 6 +SPRITE_POINTER_START//

.const FILLED_SPRITE = 127
.const NR_SPRITES = 48 

.const SPRITE_POINTERS = SCREEN_START + $03f8
/*========================= =========================================*/

.const MAX_NR_BULLITS = 16//

.const MAX_SPEED = 6
.const MIN_SPEED = 249

.const BULLIT_Y_MAX = 200;

.const MISSILE_SPEED = 8

/*========================= CHARS AND STARS  =========================================*/
.const CAT_CHAR = 27
.const CREDIT_CHAR = 30
.const BULLIT_CHAR = 28
.const SHIELD_CHAR = 29
.const INDICATOR_CHAR = 31
.const FILLED_CHAR = 34

.const METAL_CHARS_START = $60
.const DIST_LEFT_CHAR = $60
.const DIST_RIGHT_CHAR = $61

.const METAL_BORDER_CHAR = 35
.const METAL_CHAR = 36

.const BULLIT_CHAR_START = 58 

.const STAR_CHARS_START = $50 //BULLIT_CHAR_START  + 16

.assert "starfield chars must start aligned to $10", mod(STAR_CHARS_START,16), 0

.const FIRST_STAR_CHAR_BYTE = CHARS_AT +STAR_CHARS_START*8
/*========================= =========================================*/

.const NO_FREE_ENEMY = $ff
.const NO_FREE_BULLIT = $ff
.const PLAYER_WIDTH = 16
.const PLAYER_HEIGHT = 16
.const ENEMY_WIDTH = 24

.const CURRENT_WAVE_START_VAL = $ff


.const MOVEMENT_VARS_START = $7000
.const SPEEDX = $7002

.const SPEEDY = $700A

.const POSXLO = $7012
.const POSXHI = $701A
.const POSYLO = $7022
.const POSYHI = $702A


/*========================= ZERO PAGE =========================================*/
.var MATHS_TEMP_ZP = $10 
.var BITMAP_TEMP_ZP = $10 

.const PATTERN_ZP_START = $10		// up to $1F

.const ENEMY_ZP_TMP0 = $20
.const ENEMY_ZP_TMP1 = $21
.const ENEMY_ZP_TMP2 = $22
.const ENEMY_ZP_TMP3 = $23
.const ENEMY_ZP_TMP4 = $24
.const ENEMY_ZP_TMP5 = $25

.const ZPTMP1 = $33
.const ZPTMP2 = $34
.const ZPTMP3 = $35
.const ZPTMP4 = $36
.const ZPTMP5 = $37
.const ZPTMP6 = $38
.const ZPTMP7 = $39
.const ZPTMP8 = $3a
.const ZPTMP9 = $3b
.const ZPTMPA = $3c

.const BULLIT_ZP = $40  // up to $60
.const BULLIT_COL_ZP = $60 // up to $80


// not really used by program but as a reminder
.const GOAT_TRACKER_ZP1 = $8e
.const GOAT_TRACKER_ZP2 = $8f
.const GOAT_TRACKER_ZP3 = $90

.const ZP_BULLIT_TMP = $Fc

/*========================= =========================================*/

.const NTSC = $BFFF				// 1 if NTSC detected, only works in cartridge 
.const IN_GAME_MC_1	= DARK_GREY
.const IN_GAME_MC_2	= WHITE
.const METAL_COLOR= CYAN //+ 8
//====================================================================
.const ANIM_FRAME_SPEED = 3

.import source "gameplayVars.asm"
