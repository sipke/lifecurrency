// in game values, will be set when new wave is read
nrEnemiesInWave:	
				.byte 0
nrEnemiesEnded:	.byte 0
nrEnemies:	.byte 0

// helper table to easily or the $d015 value for each possible number of enemies 
nrEnemyAliveOrValue:
	.byte 4, 4+8, 4+8+16, 4+8+16+32, 4+8+16+32+64, 4+8+16+32+64 + 128





// =============================== EDIT THESE TO MAKE NEW MOVEMENT PATTERNS
//
//================================= THE INDEXES OF THESE CAN BE USED IN WAVE DEFINITIONS
//
// they always consist of: 
//				n-number of x values (movement in x direction, can be negative (=negative is to the left)) 
//				n-number of y values (movement in y direction, can be negative (=negative is up)) 
// 		and 	n + 1 (!) values of frame count, the number of frames the movement is repeated, 
//										ending with an extra 0 to indicate end of pattern 

// 				REMEMBER: 	x and y values are 0.5 pixel values 
//							so x =2 is 1 pixel on screen, I use 0.5 pixel values to make movements smoother  
//  
//				ALSO:  Every pattern has to have the pointers set in the pointer tables below !
//		  
// 				Kick assembler knows sine, and  cosine formulas  
pleftx:	.byte 2
plefty:	.byte 0
fleft:	.byte 255
		.byte 0

prightx:	.byte -2
prighty:	.byte 0
fright:		.byte 255
			.byte 0

pdiagleftx:	.byte 4
pdiaglefty:	.byte 4
fdiagleft:		.byte 100
			.byte 0

pdiagrightx:	.byte -4
pdiagrighty:	.byte 4
fdiagright:			.byte 100
				.byte 0


p0x:
	.byte 0,0,0,0
	.fill 64, sin(6.28/i)*4
p0y:
	.byte 0,0,0,0
	.fill 64, 2

fc0:	
	.byte 32,32,32,32
	.fill 64,4	
	.byte 0	

p1x:
	.byte 4,-4 ,4,-4
p1y:
	.byte 0,4,4,4
fc1:	
	.byte 50,80,80,80,0		

patternXPointerLo:
	.byte <pleftx, <prightx,<pdiagleftx,<pdiagrightx, <p0x
patternXPointerHi:	
	.byte >pleftx, >prightx,>pdiagleftx,>pdiagrightx, >p0x

patternYPointerLo:
	.byte <plefty, <prighty,<pdiaglefty,<pdiagrighty,<p0y
patternYPointerHi:	
	.byte >plefty, >prighty,>pdiaglefty,>pdiagrighty,>p0y

framesTillNextPointerLo:
	.byte <fleft,<fright,<fdiagleft, <fdiagright,<fc0
framesTillNextPointerHi:
	.byte >fleft,>fright,>fdiagleft, >fdiagright,>fc0


//	
//========================== EDIT THESE For wave characteristics =========================================
//
// the definition of a wave 'object'
.struct      Wave {nrEnemies, waveStartPosX,        waveStartPosY,                    patternNr,               patternDelay,                enemyType}

.var wave0 = Wave(6, List().add(0,0,0,255,255,255), List().add(60,100,140,40,80,120), List().add(0,0,0,1,1,1), List().add(1,11,21,1,11,21), List().add(0,0,0,0,0,0))
.var wave1 = Wave(4, List().add(0,255,30,220),      List().add(100,110,0,0),          List().add(0,1,2,3),     List().add(10,1,20,25),      List().add(1,1,2,2))
.var wave2 = Wave(3, List().add(0,50,80),           List().add(10,10,10),             List().add(4,4,4),       List().add(1,10,21),           List().add(1,0,0))
//

//==========================================================================================================
//

.var waves = List().add(wave0,wave1, wave2)

// ================== THIS WILL READ THE WAVE objects and generate tables, DON't ALTER
.var waveDefinitionPointers= List()
// write waves
nrOfWaves:		.byte waves.size()-1

.for (var i=0; i<waves.size(); i++) {
	.eval waveDefinitionPointers.add(*)
	.var n = waves.get(i).nrEnemies
	.if(n>6) {
		.error("no more than 6 enemies in a wave!")
	}

	.byte n-1
	.for (var j =0; j< n; j++) {
		.if(waves.get(i).patternNr.get(j)>255) {
			.error("start X  must me smaller than 256!")
		}

		.byte waves.get(i).patternNr.get(j)
	}
	.for (var j =0; j< n; j++) {
		.if(waves.get(i).patternDelay.get(j)==0) {
			.error("delay can't be zero!")
		}
		.byte waves.get(i).patternDelay.get(j)
	}
	.for (var j =0; j< n; j++) {
		.if(waves.get(i).enemyType.get(j)>15) {
			.error("type must me smaller than 16!")
		}
		.byte waves.get(i).enemyType.get(j)
	}
	.for (var j =0; j< n; j++) {
		.if(waves.get(i).waveStartPosX.get(j)>255) {
			.error("start X  must me smaller than 256!")
		}
		.byte waves.get(i).waveStartPosX.get(j)
	}
	.for (var j =0; j< n; j++) {
		.if(waves.get(i).waveStartPosX.get(j)>255) {
			.error("start Y  must me smaller than 256!")
		}
		.byte waves.get(i).waveStartPosY.get(j)
	}
}

waveDefinitionPointerLo:	
	.for (var i=0; i<waveDefinitionPointers.size(); i++) {	
		.byte <waveDefinitionPointers.get(i)	
	}	
waveDefinitionPointerHi:
	.for (var i=0; i<waveDefinitionPointers.size(); i++) {	
		.byte >waveDefinitionPointers.get(i)	
	}	
//=============================================================================


