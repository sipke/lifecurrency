.macro doWin() {
	lda #1
	sta gameEnded
	lda #0
	sta doPrint
	jmp setupGameWonScreen1
}


setupGameWonScreen1:
{
	sei

	// turn off screen
	lda $d016
	and #%11011111
	sta $d016

	lda #0
	sta $d015
	
	lda #%11101000 //bit 3: bitmap at 2nd 8K, bit 4-7: second to last 1K of char
	sta $d018
	
	jsr clearBitmapAndCharColor2
	
	generateCopyCode("transfer")		
	generateCopyCode("youve")		
	generateCopyCode("done")
	generateCopyCode("it")

	lda $dd00		// vic bank 3
	and #%11111100
	sta $dd00

//	turn on screen + multicolor
	lda $d016
	ora #%00110000
	sta $d016
	
	
	lda #<won1_irq
	sta irq_pointer
	lda #>won1_irq
	sta irq_pointer + 1

	lda #<won_irq_top
	sta $fffe
	lda #>won_irq_top
	sta $ffff
	lda #<freeze
	sta $fffa
	lda #>freeze
	sta $fffb
	
	lda #00 
	sta $d012			// raster interrupt at  line 										
	lda #%00111111		// high bit rasterline =0, bitmap, 25 rows
	sta $d011										

	jmp irq_end 
}

setupGameWonScreen2:
{
	sei

	// turn off screen
	lda $d016
	and #%11011111
	sta $d016


	lda #%11101000 //bit 3: bitmap at 2nd 8K, bit 4-7: second to last 1K of char
	sta $d018

	jsr clearBitmapAndCharColor
	
	
	generateCopyCode("baby")		

	generateCopyCode("its")		
	generateCopyCode("a")
	generateCopyCode("excl")
	generateCopyCode("shell1")
	generateCopyCode("shell2")
	generateCopyCode("shell2")
	
	getRandom()
	bmi doGirl
doBoy:	
	generateCopyCode("boy")
	jmp next
doGirl:
	generateCopyCode("girl")
	
next:	

	lda $dd00		// vic bank 3
	and #%11111100
	sta $dd00

//	turn on screen + multicolor
	lda $d016
	ora #%00110000
	sta $d016

	lda #<won2_irq
	sta irq_pointer
	lda #>won2_irq
	sta irq_pointer + 1
	lda #<won_irq_top
	sta $fffe
	lda #>won_irq_top
	sta $ffff
	lda #<freeze
	sta $fffa
	lda #>freeze
	sta $fffb
	
	lda #00 
	sta $d012			// raster interrupt at  line 										
	lda #%00111111		// high bit rasterline =0, bitmap, 25 rows
	sta $d011										

	jmp irq_end 
}

won_irq_top:
	enterIrq()
	
	ldx #>won_irq_bottom
	ldy #<won_irq_bottom
	stx $FFFF
	sty $FFFE
	lda #180
	sta $D012
	lda $d011
	and #$7f
	sta $d011
	
	lda $dd00
	and #%11111100
	sta $dd00

	lda $d011
	and #%01111111	// irq line bit 9 =0
	ora #%00100000
	sta $d011

	lda $d016
	ora #%00110000	//mc on, screen on
	sta $d016

	lda #%11101000 //bit 3: bitmap at 2nd 8K, bit 4-7: second to last 1K of char
	sta $d018

	sec
	rol $D019
	jmp quitIrq

wintro2:
	lda #$2F
	sta coolDownCount

	jmp setupGameWonScreen2
	sec
	rol $D019
	jmp quitIrq


won1_irq:

	
	lda $dc01
	and #$10
	beq wintro2
	
	sec
	rol $D019
	jmp quitIrq

wintro3:

	jmp setupScoreScreen
	sec
	rol $D019
	jmp quitIrq


won2_irq:

	lda $dc01
	cmp prevJoyVal	//debounce
	sta prevJoyVal
	bne !+

	and #$10
	beq wintro3
!:
	sec
	rol $D019
	jmp quitIrq


won_irq_bottom:

	enterIrq()
	//theres rubbish at bottom of bitmap so turn off
	lda $d016
	and #%11011111
	sta $d016

	jsr MUSIC_START + 3
	jmp (irq_pointer)